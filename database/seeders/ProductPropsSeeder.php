<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Products\Domain\Models\ProductType;

class ProductPropsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
/*        $this->callOnce([
            UserSeeder::class,
            ProductCategorySeeder::class,
            ProductTypesSeeder::class,
        ]);*/

        $productTypes = ProductType::query()->get();
        $productPropsData = file_get_contents(__DIR__ . '/productProps.json');
        $productPropsData = json_decode($productPropsData, true);

        foreach ($productTypes as $productType) {
            $order = 0;
            foreach ($productPropsData[$productType->name] ?? [] as $prop) {
                $prop['order'] = ++$order;
                $prop['product_type_id'] = $productType->id;
                $values = $prop['has_values'] ?? [];
                unset($prop['has_values']);

                if (isset($prop['meta'])) {
                    $prop['meta'] = json_encode($prop['meta']);
                }

                $productPropId = DB::table('product_props')->insertGetId($prop);

                if (! empty($values)) {
                    $createdValues = [];
                    foreach ($values as $value => $title) {
                        $synonyms = explode('|', $title);
                        $title = array_shift($synonyms);
                        $createdValues[] = $this->createPropValue($productPropId, $value, $title, $synonyms);
                    }
                    DB::table('product_prop_values')->insert($createdValues);
                }
            }
        }
    }

    protected function createPropValue($propId, $value, $title = null, $synonyms = []): array
    {
        return [
            'product_prop_id' => $propId,
            'value' => $value,
            'title' => $title,
            'synonyms' => json_encode($synonyms, JSON_UNESCAPED_UNICODE),
        ];
    }
}
