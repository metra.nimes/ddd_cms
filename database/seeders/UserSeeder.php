<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        DB::table('users')->insertGetId([
            'name' => 'Admin',
            'email' => 'admin@admin.ru',
            'password' => Hash::make('admin'),
            'role' => 'admin',
            'created_at' => DB::raw('NOW()'),
        ]);
    }
}
