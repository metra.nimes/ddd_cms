<?php

namespace Database\Seeders;

use Accounts\Domain\Models\User;
use Illuminate\Database\Seeder;
use Products\Domain\Models\ProductCategory;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
/*        $this->callOnce([
            UserSeeder::class,
        ]);*/

        $categories = [];
        foreach ($this->getCategories() as $category) {
            $parent = $category['parent'] ?? '';
            unset($category['parent']);

            if (empty($parent)) {
                $categories[$category['title']] = ProductCategory::create($category);
            } else {
                $categories[$category['title']] = ProductCategory::create($category, $categories[$parent] ?? null);
            }
        }
    }

    protected function getCategories(): array
    {
        $user = User::where('email', 'sales@vsekolesa.ru')->first();

        return [
            [
                'title' => 'Авто',
                'lead_sales_id' => $user->id,
                'parent' => '',
            ],
            [
                'title' => 'Электроника',
                'lead_sales_id' => $user->id,
                'parent' => '',
            ],
            [
                'title' => 'Портативная техника',
                'lead_sales_id' => $user->id,
                'parent' => 'Электроника',
            ],
            [
                'title' => 'Фото и видео',
                'lead_sales_id' => $user->id,
                'parent' => 'Электроника',
            ]
        ];
    }
}
