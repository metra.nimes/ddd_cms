<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Pricelists\Domain\Models\ParseRule;

class RulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        foreach (self::rulesData() as $rule) {
            (new ParseRule())
                ->setAttribute('product_type_id', $rule['product_type_id'])
                ->fill($rule)
                ->save();
        }
    }

    public static function rulesData()
    {
        return json_decode(file_get_contents(__DIR__ . '/rules.json'), true);
    }
}
