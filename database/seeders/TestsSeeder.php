<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Pricelists\Domain\Models\ParseTest;

class TestsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        foreach (self::testsData() as $test) {
            (new ParseTest())
                ->setAttribute('product_type_id', $test['product_type_id'])
                ->fill($test)
                ->save();
        }
    }

    protected static function testsData()
    {
        return json_decode(file_get_contents(__DIR__ . '/tests.json'), true);
    }
}
