<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call([
            UserSeeder::class,
            ProductCategorySeeder::class,
            ProductTypesSeeder::class,
            ProductPropsSeeder::class,
            RulesSeeder::class,
            TestsSeeder::class,
        ]);
    }
}
