<?php

namespace Database\Seeders;

use Accounts\Domain\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Products\Domain\Models\ProductCategory;

class ProductTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
   /*     $this->callOnce([
            UserSeeder::class,
            ProductCategory::class,
        ]);*/

        [
            'categoryId' => $categoryId,
            'leadContentId' => $leadContentId,
            'developerId' => $developerId
        ] = $this->getAdditionalInfo();

        DB::table('product_types')->insertGetId([
            'name' => 'tire',
            'title' => 'Шины',
            'slug' => 'shiny',
            'synonyms' => json_encode([
                0 => 'Шина',
                1 => 'Шины',
                2 => 'шины',
                3 => 'Автомобильная шина',
                4 => 'Автомобильные шины',
                5 => 'Шина автомобильная',
                6 => 'Легковая шина',
                7 => 'Легковые шины',
                8 => 'Автошина',
                9 => 'Автошина летняя',
                10 => 'Автошина зимняя',
                11 => 'Автошины',
                12 => 'Автошина шипованная',
                13 => 'Летняя шина',
                14 => 'Автомобильная шина',
                15 => 'Автошина',
                16 => 'Автошина летняя',
                17 => 'Автошина шипованная',
                18 => 'Всесезонные шины',
                19 => 'Всесезонная шина',
                20 => 'Всесезонная шины',
                21 => 'Всесезонная шины',
                22 => 'Шина всесезонная',
                23 => 'Шины всесезонные',
                24 => 'Шина зимняя',
                25 => 'Шина летняя',
                26 => 'Зимние',
                27 => 'Зимние шины',
                28 => 'Зимняя',
                29 => 'Зимняя шина',
                30 => 'Колесо в сборе',
                31 => 'Летние',
                32 => 'Летняя',
                33 => 'Летние шины',
                34 => 'Летняя шины',
                35 => 'Летняя шина',
                36 => 'нешипованная',
                37 => 'шипованная',
                38 => 'без шипов',
                39 => 'Шины для гольф каров и квадроциклов',
                40 => 'Шина для квадроцикла',
                41 => 'Шина для мини трактора.',
                42 => 'Зимняя нешипованная шина',
                43 => 'Зимняя шипованная шина',
                44 => 'Зимняя нешипованная шины',
                45 => 'Зимняя шипованная шины',
                46 => 'А/шина',
                47 => 'Автомобильные зимние шины',
                48 => 'Автомобильные летние шины',
                49 => 'Шины зимние нешипованные',
                50 => 'Шины зимние шипованные',
                51 => 'Зимняя автомобильная шина',
                52 => 'автомобильная шина',
                53 => 'Зима',
                54 => 'зима',
                55 => 'лето',
            ], JSON_UNESCAPED_UNICODE),
            //'keywords' => json_encode(['шины', 'автошины', 'покрышки', 'резина'], JSON_UNESCAPED_UNICODE),
            'product_category_id' => $categoryId,
            'lead_content_id' => $leadContentId,
            'lead_developer_id' => $developerId,
        ]);
        DB::table('product_types')->insertGetId([
            'name' => 'rim',
            'title' => 'Диски',
            'slug' => 'diski',
            'synonyms' => json_encode([
                0 => 'Диск',
                1 => 'Диски',
                2 => 'Колесные',
                3 => 'Колесный',
                4 => 'Легковой',
                5 => 'Литой диск',
                6 => 'Стальной диск для квадроцикла',
                7 => 'Стальной колесные диски',
                8 => 'Штампованный диск',
                9 => 'Диск колеса стальной',
                10 => 'литой колесные диски',
                11 => 'Диск кованый',
                12 => 'Колесные диски',
                13 => 'Диски колесные',
                14 => 'Диск колесный',
                15 => 'Колесный диск',
                16 => 'Автомобильные диски',
                17 => 'Автомобильный диск',
                18 => 'Диск автомобильный',
                19 => 'Диски автомобильные',
                20 => 'Колесные',
                21 => 'Колесный',
                22 => 'Автомобильный',
                23 => 'Автомобильные',
                24 => 'Легковой',
                25 => 'Литой диск',
                26 => 'Диск литой',
                27 => 'Литые диски',
                28 => 'Диски литые',
                29 => 'Литой',
                30 => 'Литые',
                31 => 'Штампованный диск',
                32 => 'Штампованные диски',
                33 => 'Диск штампованный',
                34 => 'Диски штампованные',
                35 => 'Штампованный',
                36 => 'Штампованные',
                37 => 'Диск грузовой',
                38 => 'Диск для квадроцикла',
                39 => 'Легковой диск',
                40 => 'Штамп.',
                41 => 'Диск стальной',
                42 => 'Диск колеса литой',
                43 => 'Колесные литые диски',
                44 => 'Литые колесные диски',
                45 => 'Колесные грузовые диски',
                46 => 'Колесные штампованные диски',
                47 => 'Литой колесный диск',
                48 => 'Литой',
                49 => 'Диск колеса',
            ], JSON_UNESCAPED_UNICODE),
            //'keywords' => json_encode(['диски'], JSON_UNESCAPED_UNICODE),
            'product_category_id' => $categoryId,
            'lead_content_id' => $leadContentId,
            'lead_developer_id' => $developerId,
        ]);
        DB::table('product_types')->insertGetId([
            'name' => 'ttire',
            'title' => 'Грузовые шины',
            'slug' => 'gruzovye-shiny',
            'synonyms' => json_encode([
                0 => 'Шины грузовые',
                1 => 'Грузовые шины',
                2 => 'Ведущая ось',
                3 => 'Автошина',
                4 => 'Рулевая ось',
                5 => 'Универсальные',
                6 => 'Ось прицепа',
                7 => 'Грузовая шина',
                8 => 'Шина грузовая',
                9 => 'Грузовые',
                10 => 'Грузовая',
                11 => 'Шина',
                12 => 'Шины',
                13 => 'Автошина',
                14 => 'Грузовые шины ЯШЗ',
                15 => 'ЯШЗ',
                16 => 'Всесезонные',
            ], JSON_UNESCAPED_UNICODE),
            //'keywords' => json_encode(['шины', 'автошины', 'покрышки', 'резина'], JSON_UNESCAPED_UNICODE),
            'product_category_id' => $categoryId,
            'lead_content_id' => $leadContentId,
            'lead_developer_id' => $developerId,
        ]);
        DB::table('product_types')->insertGetId([
            'name' => 'mtire',
            'title' => 'Мотошины',
            'slug' => 'motoshiny',
            'synonyms' => json_encode([
                0 => 'мотошины',
                1 => 'Мотошины',
                2 => 'Мото',
                3 => 'Мотошины',
                4 => 'Мотошина',
            ], JSON_UNESCAPED_UNICODE),
            //'keywords' => json_encode(['шины', 'автошины', 'покрышки', 'резина'], JSON_UNESCAPED_UNICODE),
            'product_category_id' => $categoryId,
            'lead_content_id' => $leadContentId,
            'lead_developer_id' => $developerId,
        ]);
        DB::table('product_types')->insertGetId([
            'name' => 'akb',
            'title' => 'Аккумуляторы',
            'slug' => 'akb',
            'synonyms' => json_encode([
                0 => 'Автомобильный аккумулятор',
                1 => 'Аккумулятор автомобильный',
                2 => 'Аккумулятор',
                3 => 'Аккумуляторы',
                4 => 'Аккумулятор для ИБП',
                5 => 'Аккумулятор для мототехники',
                6 => 'Аккумулятор лодочный',
                7 => 'Аккумуляторная батарея',
                8 => 'Внешний аккумулятор',
                9 => 'Грузовой аккумулятор',
                10 => 'Аккумулятор грузовой',
                11 => 'Мото аккумулятор',
                12 => 'Оригинальный автомобильный аккумулятор',
                13 => 'Тяговый аккумулятор',
                14 => 'Аккумулятор для автомобиля',
                15 => 'Аккумулятор для грузовиков',
                16 => 'Аккумулятор гелевый',
            ], JSON_UNESCAPED_UNICODE),
            //'keywords' => json_encode(['аккумулятор'], JSON_UNESCAPED_UNICODE),
            'product_category_id' => $categoryId,
            'lead_content_id' => $leadContentId,
            'lead_developer_id' => $developerId,
        ]);
        DB::table('product_types')->insertGetId([
            'name' => 'vrecorder',
            'title' => 'Видеорегистраторы',
            'slug' => 'videoreg',
            'synonyms' => json_encode([
                0 => 'Видеорегистратор',
                1 => 'Автомобильный видеорегистратор',
                2 => 'Зеркало видеорегистратор с камерой заднего вида',
                3 => 'Видеорегистратор автомобильный',
                4 => 'Мотовидеорегистратор',
                5 => 'Видеорегистратор комбо',
                6 => 'Видеорегистратор зеркало',
                7 => 'Видеорегистратор с 2-мя камерами',
                8 => 'Автомобильный видеорегистратор с 2-мя камерами',
                9 => 'Видеорегистратор с радар-детектором',
                10 => 'Видеорегистратор с радаром и GPS',
                11 => 'Видеорегистратор-зеркало',
            ], JSON_UNESCAPED_UNICODE),
            //'keywords' => json_encode(['видеорегистратор'], JSON_UNESCAPED_UNICODE),
            'product_category_id' => $categoryId,
            'lead_content_id' => $leadContentId,
            'lead_developer_id' => $developerId,
        ]);
    }

    protected function getAdditionalInfo(): array
    {
        $category = ProductCategory::where('title', 'Авто')->first();
        $users = User::where('email', 'content@vsekolesa.ru')
            ->orWhere('email', 'development@vsekolesa.ru')->orderBy('id', 'asc')->get();

        return [
            'categoryId' => $category->id,
            'leadContentId' => $users[0]->id ?? null,
            'developerId' => $users[1]->id ?? null,
        ];
    }
}
