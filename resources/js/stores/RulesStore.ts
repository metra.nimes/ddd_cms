import { defineStore } from 'pinia'
import { useLocalStorage } from '@vueuse/core'
import { router } from "@inertiajs/vue3";
import { dataSet } from "@/js/helpers";

import { useProductTypeStore } from "@/js/stores/ProductTypeStore";

// @ts-ignore
export const useParseRulesStore = defineStore('ParseRules', {
    state: function () {
        const ProductTypeStore = useProductTypeStore();
        return {
            productType: useLocalStorage('pinia/productType', 'tire'),
            initialRules: [],
            rules: useLocalStorage('pinia/rules/' + (ProductTypeStore.type || 'tire'), []),
            saved: false,
            apiErrors: null,
            validationErrors: {},
        };
    },
    getters: {
        hasChanges(state) {
            return JSON.stringify(state.rules) != JSON.stringify(state.initialRules);
        }
    },
    actions: {
        addRule(rule: object) {
            this.rules.push(JSON.parse(JSON.stringify(rule)));
        },
        deleteRule(index: number) {
            this.rules = this.rules.filter((a, r) => r !== index);
        },
        cloneRule(index: number) {
            const baseRule = JSON.parse(JSON.stringify(this.rules[index]));
            if (baseRule.id)
                delete baseRule.id;
            this.rules.splice(index, 0, baseRule);
        },
        swapRules(index1: number, index2: number) {
            if (!this.rules[index1] || !this.rules[index2]) return;
            let rule1 = {...this.rules[index1], execute_if: [...this.rules[index1].execute_if]};
            this.rules[index1] = {...this.rules[index2], execute_if: [...this.rules[index2].execute_if]};
            this.rules[index2] = rule1;
        },
        saveRules() {
            router.visit('/parse_rules/' + this.productType, {
                method: 'post',
                data: this.rules,
                preserveState: true,
                preserveScroll: true,
                onError: function (errors) {
                    //todo
                    console.log(errors);
                    this.apiErrors = errors;
                },
                onFinish: () => {
                    this.initialRules = JSON.parse(JSON.stringify(this.rules));
                    this.saved = true;
                    // закрываем после таймаута
                    setTimeout(() => {
                        this.saved = false;
                    }, 2000);
                }
            });


        },
        updateRule(index: number, path: string, value: any) {
            let tmpRules = JSON.parse(JSON.stringify(this.rules));
            dataSet(tmpRules, index + '.' + path, value);
            this.rules = tmpRules;
        },
        resetRules() {
            this.validationErrors = [];
            this.rules = JSON.parse(JSON.stringify(this.initialRules));
        },
        setValidationErrors(errors) {
            this.validationErrors = errors;
        }
    }
})
