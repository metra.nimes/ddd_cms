import { defineStore } from "pinia";
import { useLocalStorage } from "@vueuse/core";

// @ts-ignore
export const useProductTypeStore = defineStore("ProductType", {
    state: () => ({
        type: useLocalStorage("pinia/productType", "tire"),
    }),
});
