import {defineStore} from 'pinia'
import {useParseRulesStore} from "@/js/stores/RulesStore";
import {useProductTypeStore} from "@/js/stores/ProductTypeStore";
import axios from "axios";
import {useLocalStorage} from "@vueuse/core";

export const useParseTestsStore = defineStore('ParseTests', {
    state: () => ({
        tests: [],
        errors: [],
        tableScroll: useLocalStorage('pinia/tableScroll', [0, 0]), // X Y
        testsAreChecking: true,
        apiErrors: null,
    }),
    getters: {
        hasChanges(state) {
            return JSON.stringify(state.rules) != JSON.stringify(state.initialRules);
        },
        testsCount() {
            return Object.keys(this.tests).length;
        },
        correctCount() {
            return (this.testsAreChecking || this.apiErrors !== null) ? 0 : this.testsCount - Object.keys(this.errors || {}).length;
        },
        correctPercent() {
            return Math.round(this.correctCount / (this.testsCount || 1) * 100);
        },
    },
    actions: {
        execTests() {
            console.warn('Check tests');
            this.testsAreChecking = true;
            this.apiErrors = null;

            const RulesStore = useParseRulesStore();
            const ProductTypeStore = useProductTypeStore();

            axios.post(
                '/api/admin/request_broker',
                {
                    'method': 'requests:parse_rules.test',
                    'params': {
                        'product_type': ProductTypeStore.type,
                        'parse_rules': RulesStore.rules,
                        'tests': this.tests,
                    }
                })
                .then(result => {
                    this.testsAreChecking = false;
                    this.errors = result.data.errors || [];
                    RulesStore.setValidationErrors((result.data.validation_errors || [])['rules'] || []);
                })
                .catch(function (error){
                    /*console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);*/
                    this.testsAreChecking = false;
                    this.errors = [];
                    try {
                        this.apiErrors = error.response.data;
                    } catch (e) {
                        this.apiErrors = {error: 'Что-то пошло не так...'};
                        console.error(e);
                    }
                }.bind(this));
        },
        updateTest(id: number, value: any) {
            let tmpTests = this.tests;
            tmpTests[id] = value;
            this.tests = tmpTests;
        }
    }
})
