<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AdminController;
use App\Http\Controllers\DevController;

Route::middleware('auth')->group(function () {
    Route::get('/', fn() => inertia('Dashboard'))->name('dashboard');

    // RPC брокер, который отдаст запрос на исполнение и вернет результат
    Route::post('/api/admin/request_broker', [AdminController::class, 'requestBroker']);
});

if (env('APP_DEBUG', false)) {
    // Роуты для быстрых development-тестов
    Route::get('/dev/{action?}', [DevController::class, 'index']);
}
