<?php

use App\Helpers\Amqp;
use Pricelists\Infrastructure\Http\Api\ParseRuleController;
use Pricelists\Infrastructure\Http\Api\PricelistController;
use Products\Infrastructure\Http\Controllers\Api\ProductTypeController;

// Если будут Events-методы, то методы контроллеров
// для удобства распознавания просто называем с префиксом "on": onUpdate, onDelete и тп
Amqp::addWorkerRoute(
    'requests:product_type.list',
    [ProductTypeController::class, 'list']
);
Amqp::addWorkerRoute(
    'requests:product_type.export_props',
    [ProductTypeController::class, 'exportProps']
);
// TODO: удалить тестовый метод
Amqp::addWorkerRoute(
    'requests:product_type.export_vendors_products',
    [ProductTypeController::class, 'exportVendorsProducts']
);
//TODO: Перенести в offers keeper
Amqp::addWorkerRoute(
    'events:pricelist.parsed',
    [PricelistController::class, 'onParsed']
);
Amqp::addWorkerRoute(
    'requests:parse_rules.list',
    [ParseRuleController::class, 'list']
);
