<?php

return [
    'default' => [
        [
            'label' => 'Отчеты',
            'route' => 'dashboard',
        ],
        [
            'label' => 'Распознавание',
            'url' => '/parse_dashboard/',
            'children' => [
                [
                    'label' => 'Обзор',
                    'url' => '/parse_dashboard/'
                ],
                [
                    'label' => 'Ошибки',
                    'url' => '/parse_errors/'
                ],
                [
                    'label' => 'Параметры',
                    'url' => '/parse_params/'
                ],
                [
                    'label' => 'Правила {{ changedIcon() }}',
                    'route' => 'parse_rules.show'
                ],
                [
                    'label' => 'Тесты {{ testPercent() }}',
                    'route' => 'parse_tests.show'
                ],
            ],
        ],
        [
            'label' => 'Импорт',
            'url' => '/import/',
        ],
        [
            'label' => 'SEO',
            'url' => '/seo/',
        ],
        [
            'label' => 'Магазины',
            'url' => '/shops/',
        ],
        [
            'label' => 'Пользователи',
            'url' => '/users/',
            'route' => 'users.index',
        ],
        [
            'label' => 'Настройки',
            'route' => 'product_types.index',
            'children' => [
                [
                    'label' => 'Типы товаров',
                    'route' => 'product_types.index',
                    'extraActiveUrls' => [
                        '/product_props/',
                    ],
                ],
                [
                    'label' => 'Товарные категории',
                    'route' => 'product_categories.index',
                ],
            ],
        ],
    ],
];
