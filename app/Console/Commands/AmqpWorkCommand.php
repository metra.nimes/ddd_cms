<?php

namespace App\Console\Commands;

use Amqp;
use Bschmitt\Amqp\Consumer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Route;
use PhpAmqpLib\Exception\AMQPConnectionClosedException;
use PhpAmqpLib\Exception\AMQPIOException;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Console\Command\Command as CommandAlias;

//define('AMQP_DEBUG', true);

/**
 * Команда для запуска воркера RPC, который будет обрабатывать запросы из очереди RabbitMQ
 *
 * @example sail artisan amqp:work
 * @example sail artisan amqp:work requests:product_type.list
 */
class AmqpWorkCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'amqp:work {route=null}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start AMQP worker';

    /**
     * Execute the console command.
     * Request user supervisor config set.
     * @return int
     */
    public function handle(): int
    {
        Route::middleware('amqp')
            ->prefix('amqp')
            ->group(base_path('routes/amqp.php'));

        $route = $this->argument('route');

        if (empty($route) || $route === 'null') {
            $route = $this->ask('Enter route name');
        }

        [$exchange, $queue] = explode(':', $route);

        $methods = array_keys(\App\Helpers\Amqp::listWorkers());
        if (! in_array($route, $methods, true)) {
            $this->error('RPC method ' . $route . ' not found');

            return CommandAlias::FAILURE;
        }

        $this->info('Start consuming [' . $route . ']');

        try {
            Amqp::consume(
                $exchange !== 'events' ? $queue : '', // для events не нужна общая очередь
                static fn(AMQPMessage $message, Consumer $consumer) => Amqp::consumeCallback(
                    $message,
                    $consumer,
                    $exchange
                ),
                [
                    'routing' => str_replace('.all', '.*', $queue),
                    'exchange' => $exchange,
                    'queue_force_declare' => true,
                    'queue_durable' => true,
                    'consumer_no_ack' => $exchange !== 'events',
                    'queue_exclusive' => $exchange === 'events',
                    'persistent' => true, // required if you want to listen forever
                ]
            );
        } catch (AMQPConnectionClosedException | AMQPIOException $e) {
            $this->error($e->getMessage());
            $this->info('Reconnecting in 5 seconds...');
            sleep(5);

            return CommandAlias::FAILURE;
        }

        return CommandAlias::SUCCESS;
    }
}
