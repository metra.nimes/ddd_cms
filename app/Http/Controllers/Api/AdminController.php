<?php

namespace App\Http\Controllers\Api;

use Amqp;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use PhpAmqpLib\Exception\AMQPTimeoutException;

class AdminController extends Controller
{
    /**
     * Сделать Amqp-запрос к брокеру микросервиса
     * POST /api/admin/request_broker
     */
    public function requestBroker(Request $request): JsonResponse
    {
        $method = $request->json('method');
        $params = $request->json('params', []);

        if (empty($method)) {
            return response()->json([
                'error' => 'Method is empty'
            ], 400);
        }

        $ajaxPolicies = config('amqp.ajaxPolicies', []);

        $isMethodAllowed = (isset($ajaxPolicies[$method])
            and (is_callable($ajaxPolicies[$method]) ? $ajaxPolicies[$method]() : $ajaxPolicies[$method]));
        if (! $isMethodAllowed) {
            return response()->json([
                'error' => 'Method is not allowed'
            ], 400);
        }

        try {
            $response = Amqp::publishRpc($method, $params, 3);
        } catch (AMQPTimeoutException $e) { /** @phpstan-ignore-line */
            return response()->json([
                'error' => 'RPC timeout',
                'description' => $e->getMessage()
            ], 400);
        }


        return response()->json($response);
    }
}
