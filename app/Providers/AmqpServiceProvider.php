<?php

namespace App\Providers;

/**
 * @see  \App\Helpers\Amqp
 */
class AmqpServiceProvider extends \Bschmitt\Amqp\AmqpServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->app->bind('Amqp', \App\Helpers\Amqp::class);
        if (!class_exists('Amqp')) {
            class_alias(\Bschmitt\Amqp\Facades\Amqp::class, 'Amqp')/**
             * @method static consume(string $param, $param1, \Closure $param2, array $array)
             */;
        }

        $this->publishes([
            __DIR__ . '/../config/amqp.php' => config_path('amqp.php'),
        ]);
    }
}
