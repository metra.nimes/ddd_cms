<?php

namespace App\Helpers;

use Bschmitt\Amqp\Consumer;
use Bschmitt\Amqp\Exception\Configuration;
use Bschmitt\Amqp\Message;
use Bschmitt\Amqp\Publisher;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use JsonException;
use PhpAmqpLib\Exception\AMQPHeartbeatMissedException;
use PhpAmqpLib\Message\AMQPMessage;

class Amqp extends \Bschmitt\Amqp\Amqp
{
    /**
     * @var array Список методов, обрабатывающих события
     */
    protected static array $workerRoutes = [];

    /**
     * Добавить метод обрабатывающий RPC-запрос
     *
     * @param string $route
     * @param string|callable|array $action Класс, метод или функция (просто callable не работает)
     */
    public static function addWorkerRoute(string $route, string|callable|array $action): void
    {
        self::$workerRoutes[$route] = $action;
    }

    public static function listWorkers(): array
    {
        return self::$workerRoutes;
    }

    /**
     * Отправить сообщение
     *
     * @param $routing
     * @param $message
     * @param array $properties
     * @return bool|null
     * @throws Configuration
     *
     * @example Amqp::publish('requests:product_type.list', ['name' => 'tire']);
     * @example Amqp::publish('events:product_type.created',
     *  ['id' => 12, 'title' => 'Смартфоны'], ['exchange' => 'custom_exchange']
     * );
     */
    public function publish($routing, $message, array $properties = []): ?bool
    {
        $routing_parts = explode(':', $routing);
        if (count($routing_parts) < 2) {
            $routing_parts = [env('AMQP_EXCHANGE', 'events'), $routing];
        }

        try {
            $properties = array_merge([
            'content_type' => 'application/json',
            'exchange' => $routing_parts[0],
            ], $properties);
            $message =  json_encode($message, JSON_THROW_ON_ERROR);

            $ret = parent::publish($routing_parts[1], $message, $properties);
        } catch (AMQPHeartbeatMissedException) {
            // reconnect to RabbitMQ
            $publisher = App::make(Publisher::class);
            $publisher->connect();

            $ret = parent::publish($routing_parts[1], $message, $properties);
        } catch (JsonException) {
            $ret = null;
        }

        return $ret;
    }

    /**
     * Вызвать RPC-метод и получить результат
     *
     * @docs https://www.rabbitmq.com/tutorials/tutorial-six-php.html
     *
     * @param string $routing
     * @param mixed $message
     * @param int $timeout
     * @return mixed|null
     * @throws BindingResolutionException|JsonException
     * @example  $response = Amqp::publishRpc('requests:product_type.list', ['name'=>'tire'], 10);
     *
     * @example $response = Amqp::publishRpc('requests:ping-queue', 'ping', 10)
     */
    public function publishRpc(string $routing, mixed $message, int $timeout = 30): mixed
    {
        $routingParts = explode(':', $routing);
        if (count($routingParts) < 2) {
            $exchange = env('AMQP_EXCHANGE', 'events');
            $routingKey = $routingParts[0];
        } else {
            [$exchange, $routingKey] = $routingParts;
        }

        /* @var Publisher $publisher */
        $publisher = app()->make(Publisher::class);
        $publisher->connect();
        $publisher->getConnection()->set_close_on_destruct();
        $replyTo = $publisher->getChannel()->queue_declare(
            '',
            false,
            true,
            true
        );
        $replyTo = $replyTo[0];
        $publisher->getChannel()->queue_declare(
            $routingKey,
            $publisher->getProperty('queue_passive'),
            $publisher->getProperty('queue_durable'),
            $publisher->getProperty('queue_exclusive'),
            $publisher->getProperty('queue_auto_delete')
        );
        $response = false;
        $publisher->getChannel()->basic_consume(
            $replyTo,
            $publisher->getProperty('consumer_tag'),
            $publisher->getProperty('consumer_no_local'),
            $publisher->getProperty('consumer_no_ack'),
            $publisher->getProperty('consumer_exclusive'),
            $publisher->getProperty('consumer_nowait'),
            function ($message) use (&$response) {
                $response = $message;
            }
        );
        //$publisher->getChannel()->queue_bind($queue, $exchange, $queue);
        $publisher->getChannel()->basic_publish(
            new Message(
                json_encode($message, JSON_THROW_ON_ERROR),
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 2,
                    'reply_to' => $replyTo,
                    //Вместо 'correlation_id' используем эксклюзивную очередь для ответа
                ],
            ),
            $exchange,
            $routingKey
        );
        $publisher->getChannel()->wait(null, false, $timeout);


        return $response ? json_decode($response->getBody(), true, 512, JSON_THROW_ON_ERROR) : null;
    }

    public function consumeCallback(AMQPMessage $message, Consumer $consumer, $exchange = null): void
    {
        try {
            $data = json_decode($message->getBody(), true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException) {
            $data = $message->getBody();
        }

        // if it's not a json when interpret as string
        if (json_last_error() !== JSON_ERROR_NONE) {
            $data = $message->getBody();
        }

        if (is_null($exchange)) {
            $exchange = env('AMQP_EXCHANGE', 'events');
        }

        $result = self::execWorker($exchange . ':' . $message->getRoutingKey(), $data); // return another exchange

        // stop if it's an event, or we don't have reply_to
        if ($exchange === 'events' || ! $message->has('reply_to')) {
            //$consumer->acknowledge($message);
            return;
        }

        $correlationId = $message->has('correlation_id') ? $message->get('correlation_id') : null;
        $consumer->getChannel()->basic_publish(
            new AMQPMessage(
                $result,
                [
                    'content_type' => 'application/json',
                    'delivery_mode' => 2,
                    'correlation_id' => $correlationId,
                ]
            ),
            '',
            $message->get('reply_to')
        );
    }

    /**
     * @param $route
     * @param mixed $data
     * @return string|null
     */
    protected static function execWorker($route, mixed $data): string|null
    {
        $methods = self::listWorkers();
        if ($method = Arr::get($methods, $route)) {
            try {
                $myRequest = new Request();
                $myRequest->setMethod('POST');
                $myRequest->request->add($data);

                /** @var JsonResponse $result */
                $result = (new $method[0]())->{$method[1]}($myRequest);
            } catch (Exception $e) {
                $result = response()->json(['error' => $e->getMessage(), 'log' => $e->getTraceAsString()]);
            }
        } else {
            $result = response()->json(['error ' => "Method $route not found"]);
        }

        return $result->getContent();
    }
}
