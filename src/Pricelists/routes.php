<?php

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Pricelists\Application\Http\Controllers\ParseRuleController;
use Pricelists\Application\Http\Controllers\ParseTestController;

Route::middleware(['web', 'auth'])->group(function () {
    // Парсинг прайс-листов
    Route::get('parse_dashboard', fn() => inertia('Dashboard', []));
    Route::get('parse_errors', fn() => inertia('Dashboard', []));
    Route::get('parse_params', fn() => inertia('Dashboard', []));

    // Правила парсинга прайс-листов
    Route::resource('parse_rules', ParseRuleController::class)->except(
        ['store', 'edit', 'create']
    )->parameters([
            'parse_rules' => 'product_type',
        ])->missing(function (Request $request) {
            return Redirect::route('parse_rules.index');
        });
    Route::post(
        '/parse_rules/{product_type}',
        [ParseRuleController::class, 'storeMultiple']
    );

    // Тесты распознавания
    Route::resource('parse_tests', ParseTestController::class)->except(
        ['store', 'edit', 'create']
    )->parameters([
            'parse_tests' => 'product_type',
        ])->missing(function (Request $request) {
            return Redirect::route('parse_tests.index');
        });
    Route::post('/parse_tests/{product_type}', [ParseTestController::class, 'store']);
});
