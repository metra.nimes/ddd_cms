<?php

namespace Pricelists\Tests\Feature\Service;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Pricelists\Contracts\DataTransferObjects\ParseRuleDto;
use Pricelists\Contracts\DataTransferObjects\ParseTestDto;
use Pricelists\Domain\Models\ParseRule;
use Pricelists\Domain\Models\ParseTest;
use Pricelists\Domain\Services\ParseTestService;
use Tests\TestCase;

class ParseTestServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var ?ParseTestService
     */
    private $parseTestService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->parseTestService = new ParseTestService();
    }

    /** @test */
    public function itCanGetParseTestById()
    {
        // Создаем тест моделью
        $test = $this->createNameTest('Nokian Nordman 7', ['vendor' => 'Nokian', 'model' => 'Nordman 7']);

        // Получаем через сервис
        $testDto = $this->parseTestService->getById($test->id);

        // Проверяем правильность данных
        $this->assertInstanceOf(ParseTestDto::class, $testDto);
        $this->assertEquals('Nokian Nordman 7', $testDto->data['name']);
        $this->assertEquals('Nokian', $testDto->expected['vendor']);
    }

    /** @test */
    public function itCanListParseTestsOfAProductType()
    {
        // Создаем 2 теста основного типа товаров
        $this->createNameTest('Тест 1', [], 1);
        $this->createNameTest('Тест 2', [], 1);
        // ... и 1 тест дополнительного типа товаров
        $this->createNameTest('Не нужный тест', [], 2);

        // Получаем список
        $list = $this->parseTestService->list(1);

        // Проверяем правильность полученных данных
        $this->assertIsArray($list);
        $this->assertCount(2, $list);
        $this->assertInstanceOf(ParseTestDto::class, $list[0]);
        $this->assertEquals('Тест 1', $list[0]->data['name']);
        $this->assertEquals('Тест 2', $list[1]->data['name']);
    }

    /** @test */
    public function itDoesntCareIfProductTypeActuallyExitst()
    {
        // Получаем список для несуществующего продукта
        $list = $this->parseTestService->list(10000);

        // Никаких ошибок — просто пустой список
        $this->assertEquals([], $list);
    }

    /** @test */
    public function itCanCreateParseTests()
    {
        // Создаем тест
        $testDto = $this->parseTestService->create(
            productTypeId: 1,
            type: 'name',
            data: ['name' => 'Nokian Nordman 7'],
            expected: ['vendor' => 'Nokian', 'model' => 'Norman 7']
        );

        // Проверяем полученные данные
        $this->assertInstanceOf(ParseTestDto::class, $testDto);
        $this->assertEquals(1, $testDto->product_type_id);
        $this->assertEquals('Nokian Nordman 7', $testDto->data['name']);
        $this->assertEquals('Nokian', $testDto->expected['vendor']);

        // Проверяем, что создалось в БД
        $this->assertDatabaseHas('parse_tests', ['id' => $testDto->id]);
    }

    /** @test */
    public function itCanUpdateParseTests()
    {
        // Создаем тест моделью
        $model = $this->createNameTest('Nokian Nordman 7', ['vendor' => 'Nokian', 'model' => 'Nordman 8']);

        // Редактируем сервисом
        $testDto = $this->parseTestService->update($model->id, type: 'data', data: ['name' => 'Nokian Nordman 8']);

        // Проверяем правильность полученных данных
        $this->assertInstanceOf(ParseTestDto::class, $testDto);
        $this->assertEquals('data', $testDto->type);
        $this->assertEquals('Nokian Nordman 8', $testDto->data['name']);

        // Проверяем в БД
        $this->assertDatabaseHas('parse_tests', ['id' => $testDto->id, 'type' => 'data']);
    }

    /** @test */
    public function itCanDeleteParseTests()
    {
        // Создаем тест моделью
        $model = $this->createNameTest('Nokian Nordman 7', ['vendor' => 'Nokian', 'model' => 'Nordman 7']);

        // Удаляем сервисом
        $this->parseTestService->delete($model->id);

        // Проверяем в БД
        $this->assertDatabaseMissing('parse_tests', ['id' => $model->id]);
    }

    protected function createNameTest(string $name, array $expected, int $productTypeId = 1): ParseTest
    {
        $test = (new ParseTest())->setAttribute('product_type_id', $productTypeId)->fill([
            'type' => 'name',
            'data' => ['name' => $name],
            'expected' => $expected
        ]);
        $test->save();

        return $test;
    }
}
