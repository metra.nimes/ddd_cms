<?php

namespace Pricelists\Tests\Feature\Service;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Pricelists\Contracts\DataTransferObjects\ParseRuleDto;
use Pricelists\Domain\Models\ParseRule;
use Pricelists\Domain\Services\ParseRuleService;
use Tests\TestCase;

class ParseRuleServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var ?ParseRuleService
     */
    private $parseRuleService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->parseRuleService = new ParseRuleService();
    }

    /** @test */
    public function itCanGetParseRuleById()
    {
        // Создаем моделью
        $rule = $this->createRule('Тестовое правило');

        // Получаем сервисом
        $ruleDto = $this->parseRuleService->getById($rule->id);

        // Проверяем правильность данных
        $this->assertInstanceOf(ParseRuleDto::class, $ruleDto);
        $this->assertEquals('Тестовое правило', $ruleDto->title);
    }

    /** @test */
    public function itCanGetParseRulesForAProductType()
    {
        // Создаем несколько правил моделями для основного типа товара
        $this->createRule('Правило 1', 1, ['order' => 0]);
        $this->createRule('Правило 2', 1, ['order' => 1]);
        // ... и для другого типа товара, чтобы отсеять его
        $this->createRule('Лишнее правило', 2);
        // ... и ещё одно для основного типа товара
        $this->createRule('Правило 3', 1, ['order' => 2]);

        // Получаем список для основного типа товара
        $list = $this->parseRuleService->list(1);

        // Проверяем полученные данные
        $this->assertIsArray($list);
        $this->assertCount(3, $list);
        $this->assertInstanceOf(ParseRuleDto::class, $list[0]);
        $this->assertEquals('Правило 1', $list[0]->title);
        $this->assertEquals('Правило 2', $list[1]->title);
        $this->assertEquals('Правило 3', $list[2]->title);
    }

    /** @test */
    public function itDoesntCareIfProductTypeActuallyExitst()
    {
        // Получаем список для несуществующего продукта
        $list = $this->parseRuleService->list(10000);

        // Никаких ошибок — просто пустой список
        $this->assertEquals([], $list);
    }

    /** @test */
    public function itCanSetParseRulesForAProductType()
    {
        // Устанавливаем правила
        $initial = [
            new ParseRuleDto(
            // Поле не должно быть заполнено, проставляем его специально
                id: 100000,
                // Поле не должно быть заполнено, проставляем его специально
                product_type_id: 100000,
                title: 'Правило 1',
                type: 'replace',
                execute_if: [['price', 'name', 'regex', '[a-z]+']],
                field_type: 'price',
                field: 'name',
                regex: '[a-z]+',
                remove_found: false,
                replaces: '[a-z]+=>$0',
                note: 'Примечание 1'
            ),
            new ParseRuleDto(
            // Поле не должно быть заполнено, проставляем его специально
                id: 100001,
                // Поле не должно быть заполнено, проставляем его специально
                product_type_id: 100001,
                title: 'Правило 2',
                type: 'set_value',
                execute_if: [['price', 'name', 'regex', '[a-zA-Z]+']],
                field_type: 'name',
                field: 'price',
                remove_found: true,
                replaces: '[a-z]+=>$1',
                note: 'Примечание 2'
            )
        ];
        $this->parseRuleService->setList(1, $initial);

        // Получаем правила данного типа
        $result = ParseRule::query()->where('product_type_id', 1)->get();

        // Проверяем данные
        $this->assertCount(2, $result);
        $this->assertEquals('Правило 1', $result[0]['title']);
        $this->assertEquals('replace', $result[0]['type']);
        $this->assertEquals([['price', 'name', 'regex', '[a-z]+']], $result[0]['execute_if']);
        $this->assertEquals('price', $result[0]['field_type']);
        $this->assertEquals('name', $result[0]['field']);
        $this->assertEquals('[a-z]+', $result[0]['regex']);
        $this->assertEquals(false, $result[0]['remove_found']);
        $this->assertEquals('[a-z]+=>$0', $result[0]['replaces']);
        $this->assertEquals('Примечание 1', $result[0]['note']);
        $this->assertEquals('Правило 2', $result[1]['title']);
    }

    protected function createRule(string $title, int $productTypeId = 1, array $atts = []): ParseRule
    {
        $rule = (new ParseRule())
            ->setAttribute('product_type_id', $productTypeId)
            ->fill([
                'title' => $title,
                'type' => 'replace',
                ...$atts
            ]);
        $rule->save();

        return $rule;
    }
}
