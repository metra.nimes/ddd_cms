<?php

namespace Pricelists\Application\Http\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Helpers\Navigation;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Pricelists\Contracts\DataTransferObjects\ParseRuleDto;
use Pricelists\Domain\Services\ParseRuleService;
use Pricelists\Domain\Services\ParseTestService;
use Products\Contracts\DataTransferObjects\ProductTypeDto;
use Products\Domain\Services\ProductTypeService;

class ParseRuleController extends Controller
{
    public function __construct(
        private ProductTypeService $productTypeService,
        private ParseRuleService $parseRuleService,
        private ParseTestService $parseTestService
    ) {
    }

    /**
     * Редирект на первый тип товара
     * GET /parse_rules/
     */
    public function index(Request $request): RedirectResponse
    {
        try {
            $productTypeDto = $this->productTypeService->getFirst();
            abort_if($request->user()->cannot('view', $productTypeDto), 403);
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        return Redirect::route('parse_rules.show', ['product_type' => $productTypeDto->name]);
    }

    /**
     * Список правил распознавания (по категории)
     * GET /parse_rules/{product_type}
     */
    public function show(Request $request, string $productType)
    {
        try {
            $productTypeDto = $this->productTypeService->getByName($productType);
            abort_if($request->user()->cannot('view', $productTypeDto), 403);
            $this->addProductTypeSwitcher();
            // Получаем список правил
            $parseRules = $this->parseRuleService->list($productTypeDto->id);
            // Получаем список тестов
            $parseTests = $this->parseTestService->list($productTypeDto->id);
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        return Inertia::render('ParseRules/Index', [
            'rules' => $parseRules,
            'tests' => $parseTests,
        ]);
    }

    /**
     * Сохранить список правил парсинга
     * POST /parse_rules/{product_type}
     */
    public function storeMultiple(Request $request, string $productType): RedirectResponse
    {
        try {
            $productTypeDto = $this->productTypeService->getByName($productType);
            abort_if($request->user()->cannot('update', $productTypeDto), 403);
            // Собираем правила в единый формат
            $requestRules = $request->all();
            $resultRules = [];
            foreach ($requestRules as $rule) {
                $resultRules[] = new ParseRuleDto(
                    // Сохраняем на будущее, возможно, когда-то в будущем будем не целиком заменять, а точечно обновлять
                    id: data_get($rule, 'id'),
                    product_type_id: $productTypeDto->id,
                    title: data_get($rule, 'title'),
                    type: data_get($rule, 'type'),
                    execute_if: data_get($rule, 'execute_if'),
                    field_type: data_get($rule, 'field_type'),
                    field: data_get($rule, 'field'),
                    regex: data_get($rule, 'regex'),
                    remove_found: data_get($rule, 'remove_found'),
                    replaces: data_get($rule, 'replaces'),
                    note: data_get($rule, 'note')
                );
            }
            $this->parseRuleService->setList($productTypeDto->id, $resultRules);
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        // TODO Отправлять событие
        //Amqp::publish('events:parse_rules.updated', [
        //    'product_type' => $productType,
        //    'parse_rules' => ParseRuleResource::collection(
        //        ParseRule::whereBelongsTo($productType)->orderBy('order')->get()
        //    ),
        //]);

        return Redirect::route('parse_rules.show', ['product_type' => $productTypeDto->name]);
    }

    /**
     * Добавить переключатель типа товара в шапку
     * @return void
     */
    protected function addProductTypeSwitcher()
    {
        $productTypes = collect($this->productTypeService->list(perPage: 0)->data)
            ->keyBy('name')
            ->map(fn(ProductTypeDto $pType) => $pType->title)
            ->toArray();

        Navigation::addSwitcher('product_type', $productTypes, 'Выберите тип товара');
    }
}
