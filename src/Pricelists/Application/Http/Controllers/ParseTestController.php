<?php

namespace Pricelists\Application\Http\Controllers;

use App\Exceptions\EntityNotFoundException;
use App\Helpers\Navigation;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Pricelists\Domain\Models\ParseTest;
use Pricelists\Domain\Services\ParseRuleService;
use Pricelists\Domain\Services\ParseTestService;
use Products\Contracts\DataTransferObjects\ProductPropDto;
use Products\Contracts\DataTransferObjects\ProductTypeDto;
use Products\Domain\Services\ProductPropService;
use Products\Domain\Services\ProductTypeService;

class ParseTestController extends Controller
{
    public function __construct(
        private ProductTypeService $productTypeService,
        private ProductPropService $productPropService,
        private ParseTestService $parseTestService,
        private ParseRuleService $parseRuleService
    ) {
    }

    /**
     * Редирект на список тестов в первом типе продукта
     * GET /parse_tests/
     */
    public function index(Request $request): RedirectResponse
    {
        try {
            $productTypeDto = $this->productTypeService->getFirst();
            abort_if($request->user()->cannot('view', $productTypeDto), 403);
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        return Redirect::route('parse_tests.show', ['product_type' => $productTypeDto->name]);
    }

    /**
     * Список тестов распознавания (по категории)
     * GET /parse_tests/{product_type}
     */
    public function show(Request $request, string $productType)
    {
        try {
            $productTypeDto = $this->productTypeService->getByName($productType);
            abort_if($request->user()->cannot('view', $productTypeDto), 403);
            $this->addProductTypeSwitcher();
            // Получаем список тестов
            $parseTests = $this->parseTestService->list($productTypeDto->id);
            // Получаем список характеристик, которые могут встречаться в прайс-листах
            $productProps = $this->productPropService->list($productTypeDto->id, isPricelistProp: true);
            // Получаем список правил распознавания
            $parseRules = $this->parseRuleService->list($productTypeDto->id);
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        return Inertia::render('ParseTests/Index', [
            'tests' => $parseTests,
            'params' => collect($productProps)->map(fn(ProductPropDto $prop) => [
                'id' => $prop->id,
                'slug' => $prop->slug,
                'type' => $prop->type,
                'title' => $prop->title,
            ])->toArray(),
            'rules' => $parseRules,
        ]);
    }

    /**
     * Сохранение созданного теста распознавания
     * POST /parse_tests/{product_type}
     */
    public function store(Request $request, string $productType): RedirectResponse
    {
        try {
            $productTypeDto = $this->productTypeService->getByName($productType);
            abort_if($request->user()->cannot('update', $productTypeDto), 403);
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        // Set tests
        $test = $request->all();
        abort_if(empty($test), 400, 'Empty body');

        $test['product_type_id'] = $productTypeDto->id;
        $testObj = ParseTest::create($test);

        return Redirect::route('parse_tests.show', ['product_type' => $productTypeDto->name]);
    }

    /**
     * Сохранение редактируемого теста распознавания
     * PUT /parse_tests/{product_type}/
     */
    public function update(Request $request, string $productType): RedirectResponse
    {
        try {
            $productTypeDto = $this->productTypeService->getByName($productType);
            abort_if($request->user()->cannot('update', $productTypeDto), 403);
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        // Set tests
        $test = $request->all();
        abort_if(empty($test), 400, 'Empty body');

        $test['product_type_id'] = $productTypeDto->id;
        $testObj = ParseTest::updateOrCreate(['id' => $test['id']], $test);

        return Redirect::route('parse_tests.show', ['product_type' => $productTypeDto->name]);
    }

    /**
     * Удаление теста распознавания
     * DELETE /parse_tests/{product_type}/
     */
    public function destroy(Request $request, string $productType): RedirectResponse
    {
        try {
            $productTypeDto = $this->productTypeService->getByName($productType);
            abort_if($request->user()->cannot('update', $productTypeDto), 403);
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        ParseTest::find($request->get('id'))?->delete();

        return Redirect::route('parse_tests.show', ['product_type' => $productTypeDto->name]);
    }

    /**
     * Добавить переключатель типа товара в шапку
     * @return void
     */
    protected function addProductTypeSwitcher()
    {
        $productTypes = collect($this->productTypeService->list(perPage: 0)->data)
            ->keyBy('name')
            ->map(fn(ProductTypeDto $pType) => $pType->title)
            ->toArray();

        Navigation::addSwitcher('product_type', $productTypes, 'Выберите тип товара');
    }
}
