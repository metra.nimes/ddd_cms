<?php

namespace Pricelists\Application\Http\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PricelistController extends Controller
{
    /**
     * Обработать оповещение о том, что прайс-лист был распознан
     * AMQP events:pricelist.parsed
     */
    public function onParsed(Request $request): JsonResponse
    {
        dd($request->all());
    }
}
