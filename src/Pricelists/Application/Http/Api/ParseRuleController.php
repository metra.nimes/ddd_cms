<?php

namespace Pricelists\Application\Http\Api;

use App\Exceptions\EntityNotFoundException;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Pricelists\Domain\Services\ParseRuleService;
use Products\Domain\Services\ProductTypeService;

class ParseRuleController extends Controller
{
    public function __construct(
        private ProductTypeService $productTypeService,
        private ParseRuleService $parseRuleService
    ) {
    }

    /**
     * Получить список правил распознавания для указанного типа продукта
     * AMQP requests:parse_rules.list
     */
    public function list(Request $request): JsonResponse
    {
        $productTypeName = $request->get('product_type');
        try {
            $productTypeDto = $this->productTypeService->getByName($productTypeName);
            $parseRules = $this->parseRuleService->list($productTypeDto->id);
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        // TODO Мб превращать в массив?
        return response()->json($parseRules);
    }
}
