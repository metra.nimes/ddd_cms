<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parse_rules', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_type_id')->index();
            $table->string('title');
            $table->string('type');
            $table->json('execute_if')->nullable()->default('[]');
            $table->string('field_type')->nullable();
            $table->string('field')->nullable();
            $table->mediumText('regex')->nullable();
            $table->boolean('remove_found')->nullable();
            $table->mediumText('replaces')->nullable();
            $table->mediumText('note')->nullable();
            $table->integer('order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parse_rules');
    }
};
