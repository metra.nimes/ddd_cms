<?php

namespace Pricelists\Domain\Services;

use App\Exceptions\EntityNotCreatedException;
use App\Exceptions\EntityNotUpdatedException;
use App\Helpers\DomainModelService;
use App\Exceptions\EntityNotFoundException;
use Illuminate\Database\QueryException;
use Pricelists\Contracts\DataTransferObjects\ParseTestDto;
use Pricelists\Contracts\ParseTestServiceContract;
use Pricelists\Domain\Models\ParseTest;

class ParseTestService extends DomainModelService implements ParseTestServiceContract
{
    /**
     * @inheritDoc
     */
    public function getById(int $id): ParseTestDto
    {
        $parseTest = ParseTest::find($id);
        if (! $parseTest) {
            throw new EntityNotFoundException();
        }

        return $parseTest->toDto();
    }

    /**
     * @inheritDoc
     */
    public function list(int $productTypeId): array
    {
        return ParseTest::query()
            ->where('product_type_id', $productTypeId)
            ->orderBy('id')
            ->get()
            ->map(fn(ParseTest $parseTest) => $parseTest->toDto())
            ->toArray();
    }

    /**
     * @inheritDoc
     */
    public function create(
        int $productTypeId,
        string $type,
        array $data,
        array $expected
    ): ParseTestDto {
        $parseTest = (new ParseTest())->setAttribute('product_type_id', $productTypeId);
        $this->validateAndFill($parseTest, [
            'type' => $type,
            'data' => $data,
            'expected' => $expected,
        ]);
        try {
            $parseTest->save();
        } catch (QueryException $exception) {
            throw new EntityNotCreatedException($exception->getMessage());
        }

        return $parseTest->toDto();
    }

    /**
     * @inheritDoc
     */
    public function update(
        int $id,
        ?string $type = null,
        ?array $data = null,
        ?array $expected = null
    ): ParseTestDto {
        $parseTest = ParseTest::find($id);
        if (! $parseTest) {
            throw new EntityNotFoundException();
        }
        $this->validateAndFill($parseTest, [
            'type' => $type,
            'data' => $data,
            'expected' => $expected,
        ]);
        try {
            $parseTest->save();
        } catch (QueryException $exception) {
            throw new EntityNotUpdatedException($exception->getMessage());
        }

        return $parseTest->toDto();
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id): void
    {
        $parseTest = ParseTest::find($id);
        if (! $parseTest) {
            throw new EntityNotFoundException();
        }

        $parseTest->delete();
    }
}
