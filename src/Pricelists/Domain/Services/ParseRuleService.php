<?php

namespace Pricelists\Domain\Services;

use App\Exceptions\EntityNotUpdatedException;
use App\Helpers\DomainModelService;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Pricelists\Contracts\DataTransferObjects\ParseRuleDto;
use Pricelists\Contracts\ParseRuleServiceContract;
use App\Exceptions\EntityNotFoundException;
use Pricelists\Domain\Models\ParseRule;

class ParseRuleService extends DomainModelService implements ParseRuleServiceContract
{
    /**
     * @inheritDoc
     */
    public function getById(int $id): ParseRuleDto
    {
        $parseRule = ParseRule::find($id);
        if (! $parseRule) {
            throw new EntityNotFoundException();
        }

        return $parseRule->toDto();
    }

    /**
     * @inheritDoc
     */
    public function list(int $productTypeId): array
    {
        return ParseRule::query()
            ->where('product_type_id', $productTypeId)
            ->orderBy('order')
            ->get()
            ->map(fn(ParseRule $rule) => $rule->toDto())
            ->toArray();
    }

    /**
     * @inheritDoc
     */
    public function setList(int $productTypeId, array $parseRules): void
    {
        try {
            DB::transaction(function () use ($productTypeId, $parseRules) {
                // Просто удаляем все старые записи для этого типа
                DB::delete('delete from parse_rules where product_type_id = ?', [$productTypeId]);

                // ... и заменяем новыми
                DB::table('parse_rules')->insert(
                    collect($parseRules)->map(fn(ParseRuleDto $rule, int $order) => [
                        'product_type_id' => $productTypeId,
                        'title' => $rule->title,
                        'type' => $rule->type,
                        'execute_if' => json_encode($rule->execute_if),
                        'field_type' => $rule->field_type,
                        'field' => $rule->field,
                        'regex' => $rule->regex,
                        'remove_found' => $rule->remove_found,
                        'replaces' => $rule->replaces,
                        'note' => $rule->note,
                        'order' => $order,
                    ])->toArray()
                );
            });
        } catch (QueryException $exception) {
            throw new EntityNotUpdatedException($exception->getMessage());
        }
    }
}
