<?php

namespace Pricelists\Domain\Models;

use App\Casts\Json;
use App\Helpers\DomainModel;
use Pricelists\Contracts\DataTransferObjects\ParseRuleDto;

class ParseRule extends DomainModel
{
    public function fillableRules(): array
    {
        return [
            'title' => [],
            'type' => [],
            'execute_if' => [],
            'field_type' => [],
            'field' => [],
            'regex' => [],
            'remove_found' => [],
            'replaces' => [],
            'note' => [],
            'order' => [],
        ];
    }

    public function toDto(): mixed
    {
        return new ParseRuleDto(
            id: $this->id,
            product_type_id: $this->product_type_id,
            title: $this->title,
            type: $this->type,
            execute_if: $this->execute_if,
            field_type: $this->field_type,
            field: $this->field,
            regex: $this->regex,
            remove_found: $this->remove_found,
            replaces: $this->replaces,
            note: $this->note
        );
    }

    protected $casts = [
        'execute_if' => Json::class,
    ];
}
