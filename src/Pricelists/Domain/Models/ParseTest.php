<?php

namespace Pricelists\Domain\Models;

use App\Casts\Json;
use App\Helpers\DomainModel;
use Pricelists\Contracts\DataTransferObjects\ParseTestDto;

class ParseTest extends DomainModel
{
    public function fillableRules(): array
    {
        return [
            'type' => ['required', 'string'],
            'data' => ['array'],
            'expected' => ['array'],
        ];
    }

    public function toDto(): mixed
    {
        return new ParseTestDto(
            id: $this->id,
            product_type_id: $this->product_type_id,
            type: $this->type,
            data: $this->data,
            expected: $this->expected
        );
    }

    protected $casts = [
        'data' => Json::class,
        'expected' => Json::class,
    ];
}
