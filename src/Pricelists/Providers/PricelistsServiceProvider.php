<?php

namespace Pricelists\Providers;

use Illuminate\Support\ServiceProvider;

class PricelistsServiceProvider extends ServiceProvider
{
    public function register()
    {
    }


    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes.php');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }
}
