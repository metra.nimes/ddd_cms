<?php

namespace Pricelists\Contracts\DataTransferObjects;

class ParseTestDto
{
    public function __construct(
        public readonly ?int $id,
        public readonly ?int $product_type_id,
        public readonly ?string $type,
        public readonly ?array $data = [],
        public readonly ?array $expected = []
    ) {
    }
}
