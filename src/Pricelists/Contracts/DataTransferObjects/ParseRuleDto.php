<?php

namespace Pricelists\Contracts\DataTransferObjects;

class ParseRuleDto
{
    public function __construct(
        public readonly ?int $id,
        public readonly ?int $product_type_id,
        public readonly ?string $title,
        public readonly ?string $type,
        public readonly ?array $execute_if = null,
        public readonly ?string $field_type = null,
        public readonly ?string $field = null,
        public readonly ?string $regex = null,
        public readonly ?bool $remove_found = null,
        public readonly ?string $replaces = null,
        public readonly ?string $note = null
    ) {
    }
}
