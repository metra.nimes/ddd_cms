<?php

declare(strict_types=1);

namespace Pricelists\Contracts;

use Pricelists\Contracts\DataTransferObjects\ParseTestDto;

interface ParseTestServiceContract
{
    /**
     * Получить тест по ID
     *
     * @param int $id
     *
     * @return ParseTestDto
     *
     * @throws \App\Exceptions\EntityNotFoundException
     */
    public function getById(int $id): ParseTestDto;

    /**
     * Получить список тестов для типа товаров
     *
     * @param int $productTypeId
     *
     * @return ParseTestDto[]
     *
     * @throws \App\Exceptions\EntityNotFoundException
     */
    public function list(int $productTypeId): array;

    /**
     * Создать тест распознавания
     *
     * @param int $productTypeId
     * @param string $type
     * @param array $data
     * @param array $expected
     *
     * @return ParseTestDto
     *
     * @throws \App\Exceptions\EntityNotFoundException
     */
    public function create(
        int $productTypeId,
        string $type,
        array $data,
        array $expected
    ): ParseTestDto;

    /**
     * Отредактировать тест распознавания
     *
     * @param int $id
     * @param ?string $type
     * @param ?array $data
     * @param ?array $expected
     *
     * @return ParseTestDto
     *
     * @throws \App\Exceptions\EntityNotFoundException
     */
    public function update(
        int $id,
        ?string $type = null,
        ?array $data = null,
        ?array $expected = null
    ): ParseTestDto;

    /**
     * Удалить тест распознавания
     *
     * @param int $id
     *
     * @return void
     *
     * @throws \App\Exceptions\EntityNotFoundException
     */
    public function delete(int $id): void;
}
