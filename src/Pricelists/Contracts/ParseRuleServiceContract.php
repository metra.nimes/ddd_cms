<?php

declare(strict_types=1);

namespace Pricelists\Contracts;

use Pricelists\Contracts\DataTransferObjects\ParseRuleDto;

interface ParseRuleServiceContract
{
    /**
     * Получить правило по ID
     *
     * @param int $id
     *
     * @return ParseRuleDto
     *
     * @throws \App\Exceptions\EntityNotFoundException
     */
    public function getById(int $id): ParseRuleDto;

    /**
     * Получить список правил распознавания для типа товаров
     *
     * (!) Не проверяет наличие типа товара
     *
     * @param int $productTypeId
     *
     * @return ParseRuleDto[]
     */
    public function list(int $productTypeId): array;

    /**
     * Обновить правила распознавания для типа товаров
     *
     * (!) Не проверяет наличие типа товара
     *
     * @param int $productTypeId
     * @param ParseRuleDto[] $parseRules
     *
     * @return void
     *
     * @throws \App\Exceptions\EntityNotUpdatedException;
     */
    public function setList(int $productTypeId, array $parseRules): void;
}
