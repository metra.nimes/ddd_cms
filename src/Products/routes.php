<?php

use Illuminate\Support\Facades\Route;
use Products\Application\Http\Controllers\ProductCategoryController;
use Products\Application\Http\Controllers\ProductTypeController;
use Products\Application\Http\Controllers\ProductPropController;
use Products\Application\Http\Controllers\ProductPropValueController;

Route::middleware(['web', 'auth'])->group(function () {

    // Товарные категории
    Route::resource('product_categories', ProductCategoryController::class)
        ->except(['show']);
    Route::post('/product_categories/{product_category}/move/', [ProductCategoryController::class, 'move']);

    // Типы товаров
    Route::resource('product_types', ProductTypeController::class)
        ->except(['show']);

    // Характеристики товаров
    Route::resource('product_types.product_props', ProductPropController::class)
        ->shallow()
        ->except('show');

    // Значения характеристик
    Route::resource('product_prop_values', ProductPropValueController::class)
        ->only('store', 'update', 'destroy');
});
