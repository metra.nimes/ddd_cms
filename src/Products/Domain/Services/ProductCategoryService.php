<?php

namespace Products\Domain\Services;

use App\Helpers\DomainModelService;
use App\Helpers\NestedSetTree;
use Products\Contracts\DataTransferObjects\ProductCategoryDto;
use Products\Domain\Models\ProductCategory;
use Products\Contracts\ProductCategoryServiceContract;
use App\Exceptions\EntityNotFoundException;
use App\Exceptions\EntityNotCreatedException;
use App\Exceptions\EntityNotUpdatedException;
use Illuminate\Database\QueryException;
use App\Contracts\DataTransferObjects\PaginatedListDto;

class ProductCategoryService extends DomainModelService implements ProductCategoryServiceContract
{
    /**
     * @inheritDoc
     */
    public function getById(int $id): ProductCategoryDto
    {
        $productCategory = ProductCategory::find($id);
        if (! $productCategory) {
            throw new EntityNotFoundException();
        }

        return $productCategory->toDto();
    }

    /**
     * @inheritDoc
     */
    public function list(
        int $perPage = 25,
        array $linksQueryString = [],
    ): PaginatedListDto {
        $productCategories = ProductCategory::query();

        return $this->toPaginatedListDto($productCategories, $perPage, $linksQueryString);
    }

    /**
     * @inheritDoc
     */
    public function getTree(): array
    {
        /* @var \Kalnoy\Nestedset\Collection $nodes */
        $nodes = ProductCategory::query()
            ->orderBy('_lft', 'asc')
            ->get();

        $nodesTree = $nodes->toTree(); /** @phpstan-ignore-line */

        return NestedSetTree::traverseTree($nodesTree, fn($category) => [
            'id' => $category['id'],
            'title' => $category['title'],
            'lead_sales_id' => $category['lead_sales_id'],
        ]);
    }

    /**
     * @inheritDoc
     */
    public function create(
        ?string $title,
        ?int $lead_sales_id = null
    ): ProductCategoryDto {
        $productCategory = new ProductCategory();
        $this->validateAndFill($productCategory, [
            'title' => $title,
            'lead_sales_id' => $lead_sales_id,
        ]);
        try {
            $productCategory->save();
        } catch (QueryException $exception) {
            throw new EntityNotCreatedException($exception->getMessage());
        }

        return $productCategory->toDto();
    }

    /**
     * @inheritDoc
     */
    public function update(
        int $id,
        ?string $title,
        ?int $lead_sales_id = null
    ): ProductCategoryDto {
        $productCategory = ProductCategory::find($id);
        if (! $productCategory) {
            throw new EntityNotFoundException();
        }

        $this->validateAndFill($productCategory, [
            'title' => $title,
            'lead_sales_id' => $lead_sales_id,
        ]);

        try {
            $productCategory->save();
        } catch (QueryException $exception) {
            throw new EntityNotUpdatedException($exception->getMessage());
        }

        return $productCategory->toDto();
    }

    /**
     * @inheritDoc
     */
    public function move(int $id, int $target, string $relation): ProductCategoryDto
    {
        $productCategory = ProductCategory::find($id);
        $targetCategory = ProductCategory::find($target);
        if (! $productCategory or ! $targetCategory) {
            throw new EntityNotFoundException();
        }

        switch ($relation) {
            case 'before':
                $productCategory->beforeNode($targetCategory)->save();
                break;
            case 'after':
                $productCategory->afterNode($targetCategory)->save();
                break;
            case 'inner':
                $productCategory->appendToNode($targetCategory)->save();
                break;
            default:
                throw new EntityNotUpdatedException('Wrong relation: ' . $relation);
        }

        return $productCategory->toDto();
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id): void
    {
        $productCategory = ProductCategory::find($id);
        if (! $productCategory) {
            throw new EntityNotFoundException();
        }

        // TODO Нельзя удалять категории, если они содержат подкатегории


        // TODO Отправить события
        // Amqp::publish('events:product_type.deleted', ['id' => $id]);

        $productCategory->delete();
    }
}
