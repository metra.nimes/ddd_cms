<?php

namespace Products\Domain\Services;

use App\Helpers\DomainModelService;
use Products\Contracts\DataTransferObjects\ProductPropDto;
use Products\Contracts\ProductPropServiceContract;
use App\Exceptions\EntityNotFoundException;
use App\Exceptions\EntityNotCreatedException;
use App\Exceptions\EntityNotUpdatedException;
use Illuminate\Database\QueryException;
use Products\Domain\Models\ProductProp;
use Products\Domain\Models\ProductType;

class ProductPropService extends DomainModelService implements ProductPropServiceContract
{
    /**
     * @inheritDoc
     */
    public function getById(int $id): ProductPropDto
    {
        $productProp = ProductProp::find($id);
        if (! $productProp) {
            throw new EntityNotFoundException();
        }

        return $productProp->toDto();
    }

    /**
     * @inheritDoc
     */
    public function list(
        int|string $productTypeId,
        ?string $searchQuery = null,
        ?bool $isPricelistProp = null,
        ?bool $isModelProp = null,
        ?bool $isFilterProp = null,
        string $orderBy = 'order'
    ): array {
        if (is_string($productTypeId) and ! is_numeric($productTypeId)) {
            // Получаем тип товара по системному названию
            $productTypeObj = ProductType::where('name', $productTypeId)->first();
        } else {
            // Получаем тип товара по ID
            $productTypeObj = ProductType::find($productTypeId);
        }
        if (! $productTypeObj) {
            throw new EntityNotFoundException();
        }

        $productProps = ProductProp::query()
            ->where('product_type_id', '=', $productTypeObj->id)
            ->when($isPricelistProp, function ($query) {
                $query->where('is_pricelist_prop', '=', true);
            })
            ->when($isModelProp, function ($query) {
                $query->where('is_model_prop', '=', true);
            })
            ->when($isFilterProp, function ($query) {
                $query->where('is_filter_prop', '=', true);
            })
            ->maybeSearch($searchQuery)
            ->orderBy($orderBy, 'asc');
        $productProps = $productProps
            ->get()
            ->map(fn(ProductProp $productProp) => $productProp->toDto())
            ->toArray();

        return $productProps;
    }

    /**
     * @inheritDoc
     */
    public function create(
        int $product_type_id,
        ?string $title,
        ?string $slug,
        ?string $type,
        ?bool $is_multiple = null,
        ?int $decimal_numbers = null,
        ?string $validation_regexp = null,
        ?float $min_value = null,
        ?float $max_value = null,
        ?bool $is_pricelist_prop = null,
        ?array $synonyms = [],
        ?bool $required_for_offer = null,
        ?bool $is_model_prop = null,
        ?bool $required_for_model = null,
        ?bool $is_filter_prop = null,
        ?bool $is_parent_tag = null,
        ?string $filter_title = null,
        ?string $filter_options_url = null,
        ?string $filter_options_layout = null,
        ?bool $filter_has_search = null,
        ?string $filter_options_order = null,
        ?string $filter_hint = null
    ): ProductPropDto {
        $productType = ProductType::find($product_type_id);
        if (! $productType) {
            throw new EntityNotFoundException();
        }

        $productProp = new ProductProp();

        // Также нужно дальше для валидации уникальности в рамках типа продукта
        $productProp->product_type_id = $product_type_id;

        $this->validateAndFill($productProp, [
            'title' => $title,
            'slug' => $slug,
            'type' => $type,
            'is_multiple' => $is_multiple,
            'meta->decimal_numbers' => $decimal_numbers,
            'meta->validation_regexp' => $validation_regexp,
            'meta->min_value' => $min_value,
            'meta->max_value' => $max_value,
            'is_pricelist_prop' => $is_pricelist_prop,
            'synonyms' => $synonyms,
            'required_for_offer' => $required_for_offer,
            'is_model_prop' => $is_model_prop,
            'required_for_model' => $required_for_model,
            'is_filter_prop' => $is_filter_prop,
            'is_parent_tag' => $is_parent_tag,
            'meta->filter_title' => $filter_title,
            'meta->filter_options_url' => $filter_options_url,
            'meta->filter_options_layout' => $filter_options_layout,
            'meta->filter_has_search' => $filter_has_search,
            'filter_options_order' => $filter_options_order,
            'meta->filter_hint' => $filter_hint,
        ]);

        try {
            $productProp->save();
        } catch (QueryException $exception) {
            throw new EntityNotCreatedException($exception->getMessage());
        }

        // TODO Отправить события
        //Amqp::publish('events:product_type.updated.props', [
        //    'id' => $productTypeDto->id,
        //    'attr' => 'values',
        //    'new_value' => $productTypeDto->getProps(),
        //    'updated_at' => $productTypeDto->updated_at,
        //]);

        return $productProp->toDto();
    }

    /**
     * @inheritDoc
     */
    public function update(
        int $id,
        ?string $title,
        ?string $slug,
        ?string $type,
        ?bool $is_multiple = null,
        ?int $decimal_numbers = null,
        ?string $validation_regexp = null,
        ?int $min_value = null,
        ?int $max_value = null,
        ?bool $is_pricelist_prop = null,
        ?array $synonyms = [],
        ?bool $required_for_offer = null,
        ?bool $is_model_prop = null,
        ?bool $required_for_model = null,
        ?bool $is_filter_prop = null,
        ?bool $is_parent_tag = null,
        ?string $filter_title = null,
        ?string $filter_options_url = null,
        ?string $filter_options_layout = null,
        ?bool $filter_has_search = null,
        ?string $filter_options_order = null,
        ?string $filter_hint = null
    ): ProductPropDto {
        $productProp = ProductProp::find($id);
        if (! $productProp) {
            throw new EntityNotFoundException();
        }
        $this->validateAndFill($productProp, [
            'title' => $title,
            'slug' => $slug,
            'type' => $type,
            'is_multiple' => $is_multiple,
            'meta->decimal_numbers' => $decimal_numbers,
            'meta->validation_regexp' => $validation_regexp,
            'meta->min_value' => $min_value,
            'meta->max_value' => $max_value,
            'is_pricelist_prop' => $is_pricelist_prop,
            'synonyms' => $synonyms,
            'required_for_offer' => $required_for_offer,
            'is_model_prop' => $is_model_prop,
            'required_for_model' => $required_for_model,
            'is_filter_prop' => $is_filter_prop,
            'is_parent_tag' => $is_parent_tag,
            'meta->filter_title' => $filter_title,
            'meta->filter_options_url' => $filter_options_url,
            'meta->filter_options_layout' => $filter_options_layout,
            'meta->filter_has_search' => $filter_has_search,
            'filter_options_order' => $filter_options_order,
            'meta->filter_hint' => $filter_hint,
        ]);
        try {
            $productProp->save();
        } catch (QueryException $exception) {
            throw new EntityNotUpdatedException($exception->getMessage());
        }

        // TODO Отправлять события
        //Amqp::publish('events:product_type.updated.props', [
        //    'id' => $productType->id,
        //    'attr' => 'values',
        //    'new_value' => $productType->getProps(),
        //    'updated_at' => $productType->updated_at,
        //]);

        return $productProp->toDto();
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id): void
    {
        $productProp = ProductProp::find($id);
        if (! $productProp) {
            throw new EntityNotFoundException();
        }

        // TODO Отправлять событие
        //Amqp::publish('events:product_type.updated.props', [
        //    'id' => $productType->id,
        //    'attr' => 'values',
        //    'new_value' => $productType->getProps(),
        //    'updated_at' => $productType->updated_at,
        //]);

        $productProp->delete();
    }
}
