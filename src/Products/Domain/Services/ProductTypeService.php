<?php

namespace Products\Domain\Services;

use App\Exceptions\EntityNotDeletedException;
use App\Helpers\DomainModelService;
use Products\Contracts\ProductTypeServiceContract;
use App\Exceptions\EntityNotFoundException;
use App\Exceptions\EntityNotCreatedException;
use App\Exceptions\EntityNotUpdatedException;
use Illuminate\Database\QueryException;
use App\Contracts\DataTransferObjects\PaginatedListDto;
use Products\Contracts\DataTransferObjects\ProductTypeDto;
use Products\Domain\Models\ProductType;

class ProductTypeService extends DomainModelService implements ProductTypeServiceContract
{
    /**
     * @inheritDoc
     */
    public function getById(int $id): ProductTypeDto
    {
        $productType = ProductType::find($id);
        if (! $productType) {
            throw new EntityNotFoundException();
        }

        return $productType->toDto();
    }

    /**
     * @inheritDoc
     */
    public function getByName(string $name): ProductTypeDto
    {
        $productType = ProductType::where('name', $name)->first();
        if (! $productType) {
            throw new EntityNotFoundException();
        }

        return $productType->toDto();
    }

    /**
     * @inheritDoc
     */
    public function getFirst(): ProductTypeDto
    {
        $productType = ProductType::first();
        if (! $productType) {
            throw new EntityNotFoundException();
        }

        return $productType->toDto();
    }

    /**
     * @inheritDoc
     */
    public function list(
        ?string $stage = null,
        ?string $searchQuery = null,
        ?string $category = null,
        ?string $orderBy = null,
        int $perPage = 25,
        array $linksQueryString = [],
    ): PaginatedListDto {
        $productTypes = ProductType::query()
            ->maybeFilterStage($stage)
            ->maybeSearch($searchQuery)
            ->maybeFilterCategory($category)
            ->orderBy($orderBy ?: 'title', 'asc');

        return $this->toPaginatedListDto($productTypes, $perPage, $linksQueryString);
    }

    /**
     * @inheritDoc
     */
    public function create(
        ?string $name,
        ?string $title,
        ?string $slug,
        ?int $product_category_id,
        ?bool $publish_unrecognized_offers = null,
        ?int $lead_content_id = null,
        ?int $lead_developer_id = null
    ): ProductTypeDto {
        $productType = new ProductType();
        $this->validateAndFill($productType, [
            'name' => $name,
            'title' => $title,
            'slug' => $slug,
            'product_category_id' => $product_category_id,
            'publish_unrecognized_offers' => $publish_unrecognized_offers,
            'lead_content_id' => $lead_content_id,
            'lead_developer_id' => $lead_developer_id,
        ]);
        try {
            $productType->save();
        } catch (QueryException $exception) {
            throw new EntityNotCreatedException($exception->getMessage());
        }

        // TODO Отправить события
        // Amqp::publish('events:product_type.created', ProductTypeResource::make($productType));

        return $productType->toDto();
    }

    /**
     * @inheritDoc
     */
    public function update(
        int $id,
        ?string $name = null,
        ?string $title = null,
        ?string $slug = null,
        ?int $product_category_id = null,
        ?array $synonyms = null,
        ?string $stage = null,
        ?bool $publish_unrecognized_offers = null,
        ?int $lead_content_id = null,
        ?int $lead_developer_id = null
    ): ProductTypeDto {
        $productType = ProductType::find($id);
        if (! $productType) {
            throw new EntityNotFoundException();
        }

        // TODO Нельзя редактировать slug опубликованных разделов

        $this->validateAndFill($productType, [
            'name' => $name,
            'title' => $title,
            'slug' => $slug,
            'product_category_id' => $product_category_id,
            'synonyms' => $synonyms,
            'stage' => $stage,
            'publish_unrecognized_offers' => $publish_unrecognized_offers,
            'lead_content_id' => $lead_content_id,
            'lead_developer_id' => $lead_developer_id,
        ]);

        try {
            $productType->save();
        } catch (QueryException $exception) {
            throw new EntityNotUpdatedException($exception->getMessage());
        }

        // TODO Отправить события
        // Amqp::publish('events:product_type.updated', ProductTypeResource::make($productType));

        return $productType->toDto();
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id): void
    {
        $productType = ProductType::find($id);
        if (! $productType) {
            throw new EntityNotFoundException();
        }

        // Опубликованные типы товаров нельзя удалять
        $publishedStages = config('models.product_types.published_stages', []);
        if (in_array($productType->stage, $publishedStages)) {
            throw new EntityNotDeletedException('Нельзя удалять опубликованные типы товаров');
        }

        // TODO Отправить события
        // Amqp::publish('events:product_type.deleted', ['id' => $id]);

        $productType->delete();
    }
}
