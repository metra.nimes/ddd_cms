<?php

namespace Products\Domain\Services;

use App\Exceptions\EntityNotDeletedException;
use App\Helpers\DomainModelService;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use App\Exceptions\EntityNotFoundException;
use App\Exceptions\EntityNotCreatedException;
use App\Exceptions\EntityNotUpdatedException;
use Products\Contracts\DataTransferObjects\ProductPropValueDto;
use Products\Contracts\ProductPropValueServiceContract;
use Products\Domain\Models\ProductProp;
use Products\Domain\Models\ProductPropValue;
use Products\Domain\Models\ProductType;

class ProductPropValueService extends DomainModelService implements ProductPropValueServiceContract
{
    /**
     * @inheritDoc
     */
    public function getById(int $id): ProductPropValueDto
    {
        $propValue = ProductPropValue::find($id);
        if (! $propValue) {
            throw new EntityNotFoundException();
        }

        return $propValue->toDto();
    }

    /**
     * @inheritDoc
     */
    public function create(
        int $productPropId,
        ?string $value,
        ?string $title = null,
        ?array $synonyms = null
    ): ProductPropValueDto {
        $productProp = ProductProp::find($productPropId);
        if (! $productProp) {
            throw new EntityNotFoundException();
        }

        $propValue = new ProductPropValue();

        // Также нужно дальше для валидации уникальности в рамках одной характеристики
        $propValue->product_prop_id = $productPropId;

        $this->validateAndFill($propValue, [
            'value' => $value,
            'title' => $title,
            'synonyms' => $synonyms,
        ]);

        try {
            $propValue->save();
        } catch (QueryException $exception) {
            throw new EntityNotCreatedException($exception->getMessage());
        }

        // TODO Отправить события
        //Amqp::publish('events:product_type.updated.props', [
        //    'id' => $productType->id,
        //    'attr' => 'values',
        //    'new_value' => $productType->getProps(),
        //    'updated_at' => $productProp->updated_at,
        //]);

        return $propValue->toDto();
    }

    /**
     * @inheritDoc
     */
    public function listPropValues(int $productPropId): array
    {
        $productProp = ProductProp::find($productPropId);
        if (! $productProp) {
            throw new EntityNotFoundException();
        }

        $propsValues = $this->listPropsValues([$productPropId]);

        return data_get($propsValues, $productPropId, []);
    }

    /**
     * @inheritDoc
     */
    public function listPropsValues(array $productPropIds): array
    {
        $productPropIds = array_map('intval', $productPropIds);
        $propsValues = ProductPropValue::query()
            ->select('id', 'product_prop_id', 'value', 'title', 'synonyms', 'is_visible')
            ->whereIn('product_prop_id', $productPropIds)
            // TODO Учитывать ручной приоритет DB::raw выражением
            ->orderBy('value', 'asc')
            ->get();
        $result = array_fill_keys($productPropIds, []);
        foreach ($propsValues as $value) {
            $result[$value->product_prop_id][] = new ProductPropValueDto(
                id: $value->id,
                product_prop_id: $value->product_prop_id,
                value: $value->value,
                title: $value->title,
                synonyms: $value->synonyms,
                is_visible: $value->is_visible
            );
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function update(
        int $id,
        ?string $value = null,
        ?string $title = null,
        ?array $synonyms = null
    ): ProductPropValueDto {
        $propValue = ProductPropValue::find($id);
        if (! $propValue) {
            throw new EntityNotFoundException();
        }

        $this->validateAndFill($propValue, [
            'value' => $value,
            'title' => $title,
            'synonyms' => $synonyms,
        ]);

        try {
            $propValue->save();
        } catch (QueryException $exception) {
            throw new EntityNotUpdatedException($exception->getMessage());
        }

        // TODO Отправить события
        // Amqp::publish('events:product_type.updated.props', [
        //            'id' => $productType->id,
        //            'attr' => 'values',
        //            'new_value' => $productType->getProps(),
        //            'updated_at' => $productProp->updated_at,
        //        ]);

        return $propValue->toDto();
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id): void
    {
        $propValue = ProductPropValue::find($id);
        if (! $propValue) {
            throw new EntityNotFoundException();
        }

        $propValue->delete();
    }

    /**
     * @inheritDoc
     */
    public function deleteMultiple(array $ids): void
    {
        $ids = array_map('intval', $ids);
        try {
            DB::table((new ProductPropValue())->getTable())->whereIn('id', $ids)->delete();
        } catch (QueryException $exception) {
            throw new EntityNotDeletedException($exception->getMessage());
        }
    }
}
