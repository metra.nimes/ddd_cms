<?php

namespace Products\Domain\Models;

use App\Casts\Json;
use App\Helpers\DomainModel;
use App\Rules\IsARegexRule;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Validation\Rule;
use Products\Contracts\DataTransferObjects\ProductPropDto;

class ProductProp extends DomainModel
{
    public function productType(): BelongsTo
    {
        return $this->belongsTo(ProductType::class);
    }

    public function values(): HasMany
    {
        return $this
            ->hasMany(ProductPropValue::class)
            // TODO Учитывать выбранный порядок сортировки
            ->orderBy('value', 'asc');
    }

    public function fillableRules(): array
    {
        $uniqueForTheProductType = Rule::unique($this->getTable())->where('product_type_id', $this->product_type_id);

        return [
            'title' => ['required', $uniqueForTheProductType, 'min:3', 'max:30'],
            'slug' => ['required', $uniqueForTheProductType, 'min:3', 'max:20'],
            'type' => ['required', Rule::in(array_keys(config('models.product_props.types', [])))],
            'is_multiple' => ['boolean'],
            'meta->decimal_numbers' => [
                'exclude_unless:type,decimal',
                'required_if:type,decimal',
                Rule::in(array_keys(config('models.product_props.decimal_numbers', []))),
            ],
            'meta->validation_regexp' => [
                // Валидное регулярное выражение
                new IsARegexRule(),
            ],
            'meta->min_value' => ['decimal:0,3'],
            'meta->max_value' => ['decimal:0,3'],
            'is_pricelist_prop' => ['boolean'],
            'synonyms' => ['array'],
            'required_for_offer' => ['boolean'],
            'is_model_prop' => ['boolean'],
            'required_for_model' => ['boolean'],
            'is_filter_prop' => ['boolean'],
            'is_parent_tag' => ['boolean'],
            'meta->filter_title' => ['min:3', 'max:30'],
            'meta->filter_options_url' => ['url'],
            'meta->filter_options_layout' => [
                Rule::in(array_keys(config('models.product_props.filter_options_layouts', []))),
            ],
            'meta->filter_has_search' => ['boolean'],
            'filter_options_order' => [Rule::in(array_keys(config('models.product_props.filter_options_orders', [])))],
            'meta->filter_hint' => ['max:255'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function toDto(): ProductPropDto
    {
        return new ProductPropDto(
            id: $this->id,
            product_type_id: $this->product_type_id,
            title: $this->title,
            slug: $this->slug,
            type: $this->type,
            is_multiple: $this->is_multiple,
            decimal_numbers: data_get($this->meta, 'decimal_numbers'),
            validation_regexp: data_get($this->meta, 'validation_regexp'),
            min_value: data_get($this->meta, 'min_value'),
            max_value: data_get($this->meta, 'max_value'),
            is_pricelist_prop: $this->is_pricelist_prop,
            synonyms: $this->synonyms,
            required_for_offer: $this->required_for_offer,
            is_model_prop: $this->is_model_prop,
            required_for_model: $this->required_for_model,
            is_filter_prop: $this->is_filter_prop,
            is_parent_tag: $this->is_parent_tag,
            filter_title: data_get($this->meta, 'filter_title'),
            filter_options_url: data_get($this->meta, 'filter_options_url'),
            filter_options_layout: data_get($this->meta, 'filter_options_layout'),
            filter_has_search: data_get($this->meta, 'filter_has_search'),
            filter_options_order: $this->filter_options_order,
            filter_hint: data_get($this->meta, 'filter_hint'),
            created_at: $this->created_at,
            updated_at: $this->updated_at
        );
    }

    protected $guarded = [
        'id',
        'product_type_id',
        // Нельзя задавать вручную для одного объекта, т.к. при перемещении меняются значения нескольких объектов
        'order',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'synonyms' => Json::class,
        'meta' => Json::class,
    ];

    public function scopeMaybeSearch($query, string|null $q)
    {
        if ($q !== null and $q !== '') {
            $query->where(function ($query) use ($q) {
                $query
                    ->orWhere('slug', 'like', "%{$q}%")
                    ->orWhere('title', 'like', "%{$q}%");
            });
            // TODO Поиск по значениям
        }
    }
}
