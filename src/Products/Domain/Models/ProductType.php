<?php

namespace Products\Domain\Models;

use App\Casts\Json;
use App\Helpers\DomainModel;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use Products\Contracts\DataTransferObjects\ProductTypeDto;

class ProductType extends DomainModel
{
    public function fillableRules(): array
    {
        return [
            'name' => ['required', 'unique:' . $this->getTable(), 'alpha', 'min:3', 'max:12'],
            'title' => ['required', 'unique:' . $this->getTable(), 'min:3', 'max:30'],
            'slug' => ['required', 'unique:' . $this->getTable(), 'min:3', 'max:20', 'alpha_dash'],
            'stage' => [Rule::in(array_keys(config('models.product_types.stages', [])))],
            'synonyms' => ['array'],
            'publish_unrecognized_offers' => ['boolean'],
            'product_category_id' => ['required', 'exists:product_categories,id'],
            // (!) Не делаем валидацию сознательно, чтобы не было связей с другой доменной областью
            'lead_content_id' => [/*'exists:users,id'*/],
            'lead_developer_id' => [/*'exists:users,id'*/],
        ];
    }

    /**
     * @inheritDoc
     */
    public function toDto(): ProductTypeDto
    {
        return new ProductTypeDto(
            id: $this->id,
            name: $this->name,
            title: $this->title,
            slug: $this->slug,
            product_category_id: $this->product_category_id,
            stage: $this->stage,
            synonyms: $this->synonyms ?: [],
            publish_unrecognized_offers: $this->publish_unrecognized_offers,
            lead_content_id: $this->lead_content_id,
            lead_developer_id: $this->lead_developer_id,
            created_at: $this->created_at,
            updated_at: $this->updated_at
        );
    }

    protected $casts = [
        'synonyms' => Json::class,
        'keywords' => Json::class,
        'meta' => Json::class,
    ];

    public function scopeMaybeFilterStage($query, string|null $stage)
    {
        if ($stage) {
            $query->where('stage', '=', $stage);
        }
    }

    public function scopeMaybeSearch($query, ?string $q)
    {
        if ($q !== null and $q !== '') {
            $query->where(function ($query) use ($q) {
                $query->where('name', 'like', "%{$q}%")
                    ->orWhere('slug', 'like', "%{$q}%")
                    ->orWhere('title', 'like', "%{$q}%");
            });
        }
    }

    public function scopeMaybeFilterCategory($query, ?string $category)
    {
        if ($category) {
            // Принадлежность к категории или одной из вложенных подкатегорий
            $selectedCategories = ProductCategory::descendantsAndSelf((int)$category) // @phpstan-ignore-line
            ->pluck('title', 'id')
                ->toArray();
            if (! empty($selectedCategories)) {
                $query->whereIn('product_category_id', array_keys($selectedCategories));
            }
        }
    }

    public function getProps(): array
    {
        $productProps = ProductProp::whereBelongsTo($this)
            ->get()
            ->map(static function ($productProp) {
                $params = [
                    /* @var ProductProp $productProp */
                    'id' => $productProp->id,
                    'slug' => $productProp->slug,
                    'type' => $productProp->type,
                    'title' => $productProp->title,
                    'is_pricelist_prop' => $productProp->is_pricelist_prop,
                    'required_for_offer' => $productProp->required_for_offer,
                    'is_multiple' => $productProp->is_multiple,
                    'synonyms' => $productProp->synonyms,
                    'values' => [],
                ];
                switch ($productProp->type) {
                    case 'int':
                        $params['min_value'] = Arr::get($productProp->meta, 'min_value');
                        $params['max_value'] = Arr::get($productProp->meta, 'max_value');
                        break;
                    case 'decimal':
                        $params['min_value'] = Arr::get($productProp->meta, 'min_value');
                        $params['max_value'] = Arr::get($productProp->meta, 'max_value');
                        $params['decimal_numbers'] = Arr::get($productProp->meta, 'decimal_numbers', 1);
                        break;
                    case 'string':
                        $params['validation_regexp'] = Arr::get($productProp->meta, 'validation_regexp');
                        break;
                }

                return $params;
            })
            // Проставляем ключи, чтобы по ним быстро проставить значения и дополнить данные
            ->keyBy('id')
            ->toArray();

        return $productProps;
    }
}
