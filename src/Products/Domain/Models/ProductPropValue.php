<?php

namespace Products\Domain\Models;

use App\Casts\Json;
use App\Helpers\DomainModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Validation\Rule;
use Products\Contracts\DataTransferObjects\ProductPropValueDto;

class ProductPropValue extends DomainModel
{
    public function productProp(): BelongsTo
    {
        return $this->belongsTo(ProductProp::class);
    }

    public function fillableRules(): array
    {
        $uniqueForTheProp = Rule::unique($this->getTable())->where('product_prop_id', $this->product_prop_id);

        return [
            'value' => ['required', $uniqueForTheProp, 'min:3', 'max:20', 'alpha_dash'],
            'title' => [$uniqueForTheProp],
            // TODO Валидация, что синонимы не повторяются
            'synonyms' => ['array'],
        ];
    }

    public function toDto(): mixed
    {
        return new ProductPropValueDto(
            id: $this->id,
            product_prop_id: $this->product_prop_id,
            value: $this->value,
            title: $this->title,
            synonyms: $this->synonyms,
            is_visible: $this->is_visible
        );
    }

    protected $casts = [
        'synonyms' => Json::class,
        'meta' => Json::class,
    ];

    protected $guarded = [
        'id',
        'product_prop_id',
        'created_at',
        'updated_at',
    ];
}
