<?php

namespace Products\Domain\Models;

use App\Casts\Json;
use App\Helpers\DomainModel;
use Kalnoy\Nestedset\NodeTrait;
use Products\Contracts\DataTransferObjects\ProductCategoryDto;

// @docs https://github.com/lazychaser/laravel-nestedset

class ProductCategory extends DomainModel
{
    use NodeTrait;

    public function fillableRules(): array
    {
        return [
            'title' => ['unique:product_types', 'min:3', 'max:30'],
            // Не делаем валидацию существования пользователя, чтобы не нарушать доменную модель
            'lead_sales_id' => [/*'exists:users,id'*/],
        ];
    }

    public function toDto(): mixed
    {
        return new ProductCategoryDto(
            id: $this->id,
            title: $this->title,
            lead_sales_id: $this->lead_sales_id,
            created_at: $this->created_at,
            updated_at: $this->updated_at
        );
    }

    protected $casts = [
        'meta' => Json::class,
    ];

    /**
     * Возвращает все элементы в виде дерева с children-элементами
     *
     * @param callable $fn ($item) Преобразование одного элемента в нужный результат
     *
     * @return array
     */
    public static function getTree($fn = null)
    {
        if ($fn === null) {
            $fn = fn($item) => $item->toArray();
        }
        $traverseTree = function ($items) use (&$traverseTree, &$fn) {
            return $items->map(function ($item) use (&$traverseTree, &$fn) {
                $resultItem = $fn($item);
                $resultItem['children'] = $traverseTree($item->children);

                return $resultItem;
            });
        };

        /* @var \Kalnoy\Nestedset\Collection $nodes */
        $nodes = self::query()->orderBy('_lft', 'asc')->get();

        /** @phpstan-ignore-next-line */
        $nodesTree = $nodes->toTree();

        return $traverseTree($nodesTree)->toArray();
    }
}
