<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_types', function (Blueprint $table) {
            $productTypeStages = config('models.product_types.stages', []);
            $table->id();
            $table->string('name', 12)->unique();
            $table->string('title', 30);
            $table->string('slug', 10)->unique();
            $table->enum('stage', array_keys($productTypeStages))->default(array_key_first($productTypeStages));
            // Синонимы в прайс-листах
            $table->json('synonyms')->nullable();
            $table->boolean('publish_unrecognized_offers')->default(0);
            $table->foreignId('product_category_id')->constrained('product_categories')->cascadeOnDelete();
            $table->integer('lead_content_id', FALSE, TRUE)->nullable();
            $table->integer('lead_developer_id', FALSE, TRUE)->nullable();
            $table->jsonb('meta')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_types');
    }
};
