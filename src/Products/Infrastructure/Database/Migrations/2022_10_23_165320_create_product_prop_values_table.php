<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_prop_values', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_prop_id')->index()->constrained('product_props')->cascadeOnDelete();
            $table->string('value', 30);
            $table->unique(['product_prop_id', 'value']);
            $table->boolean('is_visible')->default(TRUE);
            $table->string('title', 30)->nullable();
            // Синонимы в прайс-листах
            $table->json('synonyms')->nullable();
            // Ручной приоритет вместо PWS
            $table->unsignedMediumInteger('priority')->nullable();
            $table->jsonb('meta')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_prop_values');
    }
};
