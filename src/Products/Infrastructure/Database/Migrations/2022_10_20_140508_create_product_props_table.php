<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_props', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_type_id')->index()->constrained('product_types')->cascadeOnDelete();
            $table->string('title', 30);
            $table->string('slug', 10);
            $table->integer('order', FALSE, TRUE)->default(1);
            $table->unique(['product_type_id', 'slug']);
            $types = config('models.product_props.types', []);
            $table->enum('type', array_keys($types))->default(array_key_first($types));
            // Есть в прайс-листах?
            $table->boolean('is_pricelist_prop')->default(FALSE);
            // Синонимы в прайс-листах
            $table->json('synonyms')->nullable();
            // Должно быть распознано для публикации предложения
            $table->boolean('required_for_offer')->default(FALSE);
            // Характеристика модели
            $table->boolean('is_model_prop')->default(FALSE);
            // Может одновременно иметь несколько значений
            $table->boolean('is_multiple')->default(FALSE);
            // Должно быть заполнено для публикации карточки товара
            $table->boolean('required_for_model')->default(FALSE);
            // Есть в фильтрах
            $table->boolean('is_filter_prop')->default(FALSE);
            // Может содержать вложенные теги
            $table->boolean('is_parent_tag')->default(FALSE);
            // Упорядочить вложенные варианты
            $filterOptionsOrders = config('models.product_props.filter_options_orders', []);
            $table->enum('filter_options_order', array_keys($filterOptionsOrders))->default(array_key_first($filterOptionsOrders));
            // Остальное храним в мета-поле
            $table->jsonb('meta')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_props');
    }
};
