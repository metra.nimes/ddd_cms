<?php

namespace Products\Tests\Feature\Service;

use App\Contracts\DataTransferObjects\PaginatedListDto;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Products\Contracts\DataTransferObjects\ProductCategoryDto;
use Products\Domain\Models\ProductCategory;
use Products\Domain\Services\ProductCategoryService;
use Tests\TestCase;

class ProductCategoryServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var ?ProductCategoryService
     */
    private $productCategoryService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->productCategoryService = new ProductCategoryService();
    }

    /** @test */
    public function itCanCreateProductCategories()
    {
        $dto = $this->productCategoryService->create(
            title: 'Тестовая категория',
            lead_sales_id: null
        );

        // Убеждаемся, что получили правильные данные
        $this->assertInstanceOf(ProductCategoryDto::class, $dto);
        $this->assertEquals('Тестовая категория', $dto->title);

        // ..и и что данные действительно находятся в БД
        $this->assertDatabaseHas('product_categories', [
            'title' => 'Тестовая категория',
        ]);
    }

    /** @test */
    public function itCanGetProductCatgories()
    {
        // Создаем тестовый тип данных сразу моделью
        $model = ProductCategory::create([
           'title' => 'Тестовая категория 2',
        ]);

        // Получаем по ID
        $dto = $this->productCategoryService->getById($model->id);
        $this->assertInstanceOf(ProductCategoryDto::class, $dto);
        $this->assertEquals('Тестовая категория 2', $dto->title);
    }

    /** @test */
    public function itCanListProductCategories()
    {
        // Сначала категорий нет, но правильный формат
        $list = $this->productCategoryService->list();
        $this->assertInstanceOf(PaginatedListDto::class, $list);
        $this->assertIsArray($list->data);

        // Добавляем две товарных категории
        $model1 = ProductCategory::create(['title' => 'Тестовая категория 1']);
        $model2 = ProductCategory::create(['title' => 'Тестовая категория 2']);

        // Получаем обновленный список категорий
        $list = $this->productCategoryService->list();

        // Проверяем и убеждаемся, что это ровно те добавленные категории
        $this->assertInstanceOf(PaginatedListDto::class, $list);
        $this->assertIsArray($list->data);
        $this->assertNotEmpty($list->data);
        $this->assertInstanceOf(ProductCategoryDto::class, $list->data[0]);
        $this->assertEquals($model1->title, $list->data[0]->title);
        $this->assertInstanceOf(ProductCategoryDto::class, $list->data[1]);
        $this->assertEquals($model2->title, $list->data[1]->title);
    }

    /** @test */
    public function itCanListProductCategoriesAsATree()
    {
        // Сначала категорий нет, но правильный формат
        $tree = $this->productCategoryService->getTree();
        $this->assertIsArray($tree);

        // Добавляем дерево категорий
        $parentModel1 = ProductCategory::create(['title' => 'Родительская категория 1']);
        $parentModel2 = ProductCategory::create(['title' => 'Родительская категория 2']);
        $childModel1 = (new ProductCategory())->fill(['title' => 'Дочерняя категория 1'])->appendToNode($parentModel1);
        $childModel1->save();

        // Получвем обновленное дерево категорий
        $tree = $this->productCategoryService->getTree();
        $this->assertIsArray($tree);
        $this->assertNotEmpty($tree);
        $this->assertIsArray($tree[0]);
        $this->assertEquals($parentModel1->title, $tree[0]['title']);
        $this->assertIsArray($tree[0]['children']);
        $this->assertNotEmpty($tree[0]['children']);
        $this->assertEquals($childModel1->title, $tree[0]['children'][0]['title']);
        $this->assertIsArray($tree[1]);
        $this->assertEquals($parentModel2->title, $tree[1]['title']);
    }

    /** @test */
    public function itCanUpdateProductCategories()
    {
        // Создаем тестовую категорию сразу моделью
        $model = ProductCategory::create([
            'title' => 'Тестовая категория',
        ]);

        // Меняем её сервисом
        $updatedDto = $this->productCategoryService->update(
            $model->id,
            title: 'Тестовая категория 2',
        );

        // Убеждаемся, что получили правильные данные
        $this->assertInstanceOf(ProductCategoryDto::class, $updatedDto);
        $this->assertEquals('Тестовая категория 2', $updatedDto->title);

        // ..и и что данные действительно поменялись в БД
        $this->assertDatabaseHas('product_categories', [
            'id' => $model->id,
            'title' => 'Тестовая категория 2',
        ]);
    }

    /** @test */
    public function itCanMoveProductCategories()
    {
        // Добавляем дерево категорий
        $parentModel1 = ProductCategory::create(['title' => 'Родительская категория 1']);
        $parentModel2 = ProductCategory::create(['title' => 'Родительская категория 2']);
        $childModel1 = (new ProductCategory())->fill(['title' => 'Дочерняя категория 1'])->appendToNode($parentModel1);
        $childModel1->save();

        // Перемещаем категорию внутрь другого родителя
        $this->productCategoryService->move($childModel1->id, $parentModel2->id, 'inner');
        $childModel1->refresh();

        // Убеждаемся, что перенеслось
        $this->assertEquals($parentModel2->id, $childModel1->parent_id);
    }

    /** @test */
    public function itCanDeleteProductCategories()
    {
        // Создаем тестовую категорию сразу моделью
        $model = ProductCategory::create([
            'title' => 'Тестовая категория',
        ]);

        // Убеждаемся, что создалось
        $this->assertDatabaseHas('product_categories', ['id' => $model->id]);

        // Удаляем сервисом
        $this->productCategoryService->delete($model->id);

        // Убеждаемся что не существует в БД
        $this->assertDatabaseMissing('product_categories', ['id' => $model->id]);
    }
}
