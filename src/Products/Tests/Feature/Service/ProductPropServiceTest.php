<?php

namespace Products\Tests\Feature\Service;

use App\Exceptions\EntityValidationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Nette\Schema\ValidationException;
use Products\Contracts\DataTransferObjects\ProductPropDto;
use Products\Domain\Models\ProductCategory;
use Products\Domain\Models\ProductProp;
use Products\Domain\Models\ProductType;
use Products\Domain\Services\ProductPropService;
use Tests\TestCase;

class ProductPropServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var ?int ID типа товара
     */
    private $productTypeId = null;

    /**
     * @var ?ProductPropService
     */
    private $productPropService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->productPropService = new ProductPropService();

        // Для всех тестов создаем тип товара, с которым будем работать
        $productCategory = ProductCategory::create(['title' => 'Тестовая категория']);
        $productType = ProductType::create([
            'name' => 'testtype',
            'title' => 'Тестовый тип',
            'slug' => 'testtype',
            'product_category_id' => $productCategory->id,
        ]);
        $this->productTypeId = $productType->id;
    }

    protected function createProp(string $title, string $slug)
    {
        $productProp = (new ProductProp())
            ->setAttribute('product_type_id', $this->productTypeId)
            ->setAttribute('title', $title)
            ->setAttribute('slug', $slug);
        $productProp->save();

        return $productProp;
    }

    /** @test */
    public function itCanCreateProductProps()
    {
        // Создаем характеристику
        $productPropDto = $this->productPropService->create(
            product_type_id: $this->productTypeId,
            title: 'Название характеристики',
            slug: 'prop',
            type: 'string',
            is_pricelist_prop: true,
            synonyms: ['Характеристика 1', 'Характ.1'],
            required_for_offer: true,
            is_model_prop: true,
            required_for_model: true,
            is_filter_prop: true,
            is_parent_tag: true,
            filter_title: 'Название фильтра',
            filter_options_layout: 'dropdown',
            filter_has_search: true,
            filter_options_order: 'alphabet',
            filter_hint: 'Подсказка к фильтру'
        );

        // Проверяем правильность возвращенных данных
        $this->assertInstanceOf(ProductPropDto::class, $productPropDto);
        $this->assertEquals($this->productTypeId, $productPropDto->product_type_id);
        $this->assertEquals('Название характеристики', $productPropDto->title);
        $this->assertEquals('prop', $productPropDto->slug);
        $this->assertEquals('string', $productPropDto->type);
        $this->assertTrue($productPropDto->required_for_offer);
        $this->assertTrue($productPropDto->is_model_prop);
        $this->assertTrue($productPropDto->required_for_model);
        $this->assertTrue($productPropDto->is_filter_prop);
        $this->assertTrue($productPropDto->is_parent_tag);
        $this->assertEquals('Название фильтра', $productPropDto->filter_title);
        $this->assertEquals('dropdown', $productPropDto->filter_options_layout);
        $this->assertTrue($productPropDto->filter_has_search);
        $this->assertEquals('alphabet', $productPropDto->filter_options_order);
        $this->assertEquals('Подсказка к фильтру', $productPropDto->filter_hint);

        // Проверяем, что реально добавилось в БД
        $this->assertDatabaseHas('product_props', ['id' => $productPropDto->id, 'slug' => 'prop']);
    }

    /** @test */
    public function itDoesntAllowToCreateTitleDuplicates()
    {
        // Создаем характеристику
        $productProp = $this->createProp('Характеристика', 'testparam');

        // Не позволяет добавлять два параметра с одинаковым title одному типу товара
        $this->expectException(EntityValidationException::class);
        $this->productPropService->create(
            product_type_id: $this->productTypeId,
            title: 'Характеристика',
            slug: 'testparam2',
            type: 'string'
        );
    }

    /** @test */
    public function itDoesntAllowToCreateDuplicates()
    {
        // Создаем характеристику
        $productProp = $this->createProp('Характеристика', 'testparam');

        // Не позволяет добавлять два параметра с одинаковым slug одному типу товара
        $this->expectException(EntityValidationException::class);
        $this->productPropService->create(
            product_type_id: $this->productTypeId,
            title: 'Характеристика 2',
            slug: 'testparam',
            type: 'string'
        );
    }

    /** @test */
    public function itProperlyValidatesParamsDuringCreation()
    {
        // Не позволяет добавлять несуществующий тип товара
        try {
            $this->productPropService->create(
                product_type_id: $this->productTypeId,
                title: 'Характеристика',
                slug: 'testparam',
                type: 'unknowntype'
            );
            $this->fail('Валидация type не была выполнена');
        } catch (EntityValidationException $exception) {
            $this->assertArrayHasKey('type', $exception->messages);
        }

        // Не позволяет заполнять регулярное выражения для валидации строки невыполянемой регуляркой
        try {
            $this->productPropService->create(
                product_type_id: $this->productTypeId,
                title: 'Характеристика',
                slug: 'testparam',
                type: 'string',
                validation_regexp: '~hello there'
            );
            $this->fail('Валидация validation_regexp не была выполнена');
        } catch (EntityValidationException $exception) {
            $this->assertArrayHasKey('validation_regexp', $exception->messages);
        }
    }

    /** @test */
    public function itCanListProductProps()
    {
        // Добавляем две характеристики товару
        $this->createProp('Параметр 1', 'param1');
        $this->createProp('Параметр 2', 'param2');

        // Получаем список характеристик для этого товара
        $list = $this->productPropService->list($this->productTypeId);

        // Убеждаемся, что он содержит нужные данные в нужном формате
        $this->assertIsArray($list);
        $this->assertCount(2, $list);
        $this->assertInstanceOf(ProductPropDto::class, $list[0]);
        $this->assertEquals('param1', $list[0]->slug);
        $this->assertEquals('param2', $list[1]->slug);
    }

    /** @test */
    public function itCanUpdateProductProps()
    {
        // Добавляем характеристику товару
        $productProp = $this->createProp('Параметр 1', 'param1');

        // Редактируем характеристику
        $productPropDto = $this->productPropService->update(
            id: $productProp->id,
            title: 'Название характеристики',
            slug: 'prop',
            type: 'int',
            is_pricelist_prop: true,
            synonyms: ['Характеристика 1', 'Характ.1'],
            required_for_offer: true,
            is_model_prop: true,
            required_for_model: true,
            is_filter_prop: true,
            is_parent_tag: true,
            filter_title: 'Название фильтра',
            filter_options_layout: 'dropdown',
            filter_has_search: true,
            filter_options_order: 'alphabet',
            filter_hint: 'Подсказка к фильтру'
        );

        // Проверяем правильность возвращенных данных
        $this->assertInstanceOf(ProductPropDto::class, $productPropDto);
        $this->assertEquals($this->productTypeId, $productPropDto->product_type_id);
        $this->assertEquals('Название характеристики', $productPropDto->title);
        $this->assertEquals('prop', $productPropDto->slug);
        $this->assertEquals('int', $productPropDto->type);
        $this->assertTrue($productPropDto->is_pricelist_prop);
        $this->assertEquals(['Характеристика 1', 'Характ.1'], $productPropDto->synonyms);
        $this->assertTrue($productPropDto->required_for_offer);
        $this->assertTrue($productPropDto->is_model_prop);
        $this->assertTrue($productPropDto->required_for_model);
        $this->assertTrue($productPropDto->is_filter_prop);
        $this->assertTrue($productPropDto->is_parent_tag);
        $this->assertEquals('Название фильтра', $productPropDto->filter_title);
        $this->assertEquals('dropdown', $productPropDto->filter_options_layout);
        $this->assertTrue($productPropDto->filter_has_search);
        $this->assertEquals('alphabet', $productPropDto->filter_options_order);
        $this->assertEquals('Подсказка к фильтру', $productPropDto->filter_hint);

        // Проверяем, что реально добавилось в БД
        $this->assertDatabaseHas('product_props', ['id' => $productPropDto->id, 'slug' => 'prop']);
    }

    /** @test */
    public function itCanDeleteProductProps()
    {
        // Добавляем характеристику товару
        $productProp = $this->createProp('Параметр 1', 'param1');

        // Удаляем сервисом
        $this->productPropService->delete($productProp->id);

        // Убеждаемся, что удалилось
        $this->assertDatabaseMissing('product_props', ['id' => $productProp->id]);
    }
}
