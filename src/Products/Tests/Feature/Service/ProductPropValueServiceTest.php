<?php

namespace Products\Tests\Feature\Service;

use App\Exceptions\EntityValidationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Products\Contracts\DataTransferObjects\ProductPropValueDto;
use Products\Domain\Models\ProductCategory;
use Products\Domain\Models\ProductProp;
use Products\Domain\Models\ProductPropValue;
use Products\Domain\Models\ProductType;
use Products\Domain\Services\ProductPropValueService;
use Tests\TestCase;

class ProductPropValueServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var ?int ID типа товара
     */
    private $productTypeId = null;

    /**
     * @var ?int ID характеристики
     */
    private $productPropId = null;

    /**
     * @var ?ProductPropValueService
     */
    private $productPropValueService;

    protected function setUp(): void
    {
        parent::setUp();

        $this->productPropValueService = new ProductPropValueService();

        // Для всех тестов создаем тип товара и характеристику, со значениями которой будем работать
        $productCategory = ProductCategory::create(['title' => 'Тестовая категория']);
        $productType = ProductType::create([
            'name' => 'testtype',
            'title' => 'Тестовый тип',
            'slug' => 'testtype',
            'product_category_id' => $productCategory->id,
        ]);
        $this->productTypeId = $productType->id;

        $productProp = $this->createProp('Тестовая характеристика', 'test');
        $this->productPropId = $productProp->id;
    }

    protected function createProp(string $title, string $slug)
    {
        $productProp = (new ProductProp())
            ->setAttribute('product_type_id', $this->productTypeId)
            ->setAttribute('title', $title)
            ->setAttribute('slug', $slug);
        $productProp->save();

        return $productProp;
    }

    protected function createPropValue(int $productPropId, string $value, string $title = null)
    {
        $propValue = (new ProductPropValue())
            ->setAttribute('product_prop_id', $productPropId)
            ->setAttribute('value', $value)
            ->setAttribute('title', $title);
        $propValue->save();

        return $propValue;
    }

    /** @test */
    public function itCanCreateProductPropValues()
    {
        // Создаем значение характеристики
        $dto = $this->productPropValueService->create(
            productPropId: $this->productPropId,
            value: 'testvalue',
            title: 'Тестовое значение',
            synonyms: [
                'Тест',
            ]
        );

        // Проверяем полученные данные
        $this->assertInstanceOf(ProductPropValueDto::class, $dto);
        $this->assertEquals('testvalue', $dto->value);
        $this->assertEquals('Тестовое значение', $dto->title);
        $this->assertEquals(['Тест'], $dto->synonyms);

        // Проверяем, что запись существует в БД
        $this->assertDatabaseHas('product_prop_values', [
            'id' => $dto->id,
            'value' => $dto->value,
        ]);
    }

    /** @test */
    public function itDoesntAllowToCreatePropValuesThatCantBeUsedAsASlug()
    {
        // Не позволяет создавать такие значения, которые нельзя использовать в URL (в этом смысл значений)
        $this->expectException(EntityValidationException::class);
        $this->productPropValueService->create(
            productPropId: $this->productPropId,
            value: 'Тестовое значение'
        );
    }

    /** @test */
    public function itDoesntAllowToCreateDuplicateProductPropValues()
    {
        // Создаем первое значение моделью
        $this->createPropValue($this->productPropId, 'somevalue', 'Название значения');

        // Не позволяет создавать дублирующее значения
        $this->expectException(EntityValidationException::class);
        $this->productPropValueService->create(
            productPropId: $this->productPropId,
            value: 'somevalue'
        );
    }

    /** @test */
    public function itDoesntAllowToCreateDuplicateProductPropValueTitles()
    {
        // Создаем первое значение моделью
        $this->createPropValue($this->productPropId, 'somevalue', 'Название значения');

        // Не позволяет создавать дублирующие заголовки
        $this->expectException(EntityValidationException::class);
        $this->productPropValueService->create(
            productPropId: $this->productPropId,
            // Здесь уже специально другое значение
            value: 'somevalue2',
            title: 'Название значения'
        );
    }

    /** @test */
    public function itCanListProductPropValues()
    {
        // Создаем два значения
        $this->createPropValue($this->productPropId, 'value1');
        $this->createPropValue($this->productPropId, 'value2');

        // Получаем список значений
        $list = $this->productPropValueService->listPropValues($this->productPropId);

        // Проверяем полученный формат
        $this->assertIsArray($list);
        $this->assertCount(2, $list);
        $this->assertInstanceOf(ProductPropValueDto::class, $list[0]);
        $this->assertEquals('value1', $list[0]->value);
        $this->assertEquals('value2', $list[1]->value);
    }

    /** @test */
    public function itCanListProductPropValuesForMultipleProps()
    {
        // Добавляем значение в первую характеристику
        $this->createPropValue($this->productPropId, 'value1');

        // Добавляем вторую характеристику и два значения в нее
        $productProp2 = $this->createProp('Тестовая характеристика 2', 'test2');
        $this->createPropValue($productProp2->id, 'value2');
        $this->createPropValue($productProp2->id, 'value3');

        // Получаем список характеристик и значений
        $list = $this->productPropValueService->listPropsValues([$this->productPropId, $productProp2->id]);

        // Проверяем полученный формат
        $this->assertIsArray($list);
        $this->assertCount(2, $list);
        $this->assertArrayHasKey($this->productPropId, $list);
        $this->assertIsArray($list[$this->productPropId]);
        $this->assertInstanceOf(ProductPropValueDto::class, $list[$this->productPropId][0]);
        $this->assertEquals('value1', $list[$this->productPropId][0]->value);
        $this->assertArrayHasKey($productProp2->id, $list);
        $this->assertEquals('value2', $list[$productProp2->id][0]->value);
        $this->assertEquals('value3', $list[$productProp2->id][1]->value);
    }

    /** @test */
    public function itCanUpdateProductPropValues()
    {
        // Добавляем новое значение
        $propValue = $this->createPropValue($this->productPropId, 'value1');

        // Редактируем
        $this->productPropValueService->update(
            id: $propValue->id,
            value: 'value2',
            title: 'Значение 2',
            synonyms: ['знач 2']
        );

        // Получаем этот объект и проверяем
        $propValue->refresh();
        $this->assertEquals('value2', $propValue->value);
        $this->assertEquals('Значение 2', $propValue->title);
        $this->assertEquals(['знач 2'], $propValue->synonyms);
    }

    /** @test */
    public function itCanDeleteProductPropValue()
    {
        // Добавляем новое значение
        $propValue = $this->createPropValue($this->productPropId, 'value1');

        // Удаляем
        $this->productPropValueService->delete($propValue->id);

        // Убеждаемся, что нет в БД
        $this->assertDatabaseMissing('product_prop_values', ['id' => $propValue->id]);
    }

    /** @test */
    public function itCanDeleteMultipleProductPropValue()
    {
        // Добавляем два значения
        $propValue1 = $this->createPropValue($this->productPropId, 'value1');
        $propValue2 = $this->createPropValue($this->productPropId, 'value2');

        // Удаляем
        $this->productPropValueService->deleteMultiple([$propValue1->id, $propValue2->id]);

        // Убеждаемся, что нет в БД
        $this->assertDatabaseMissing('product_prop_values', ['id' => $propValue1->id]);
        $this->assertDatabaseMissing('product_prop_values', ['id' => $propValue2->id]);
    }
}
