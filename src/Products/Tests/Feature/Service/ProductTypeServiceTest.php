<?php

namespace Products\Tests\Feature\Service;

use App\Exceptions\EntityValidationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Products\Contracts\DataTransferObjects\ProductTypeDto;
use Products\Domain\Models\ProductCategory;
use Products\Domain\Models\ProductType;
use Products\Domain\Services\ProductTypeService;
use Tests\TestCase;

class ProductTypeServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var int
     */
    private $testCategoryId;

    /**
     * @var ?ProductTypeService
     */
    private $productTypeService;

    protected function setUp(): void
    {
        parent::setUp();

        $category = ProductCategory::create(['title' => 'Тестовая категория']);
        $this->testCategoryId = $category->id;
        $this->productTypeService = new ProductTypeService();
    }

    /** @test */
    public function itCanCreateProductTypes()
    {
        $dto = $this->productTypeService->create(
            name: 'test',
            title: 'Тестовый тип',
            slug: 'testtype',
            product_category_id: $this->testCategoryId
        );

        // Убеждаемся, что получили правильные данные
        $this->assertInstanceOf(ProductTypeDto::class, $dto);
        $this->assertEquals('test', $dto->name);
        $this->assertEquals('Тестовый тип', $dto->title);
        $this->assertEquals('testtype', $dto->slug);
        $this->assertEquals($this->testCategoryId, $dto->product_category_id);

        // ..и и что данные действительно находятся в БД
        $this->assertDatabaseHas('product_types', [
            'name' => 'test',
            'title' => 'Тестовый тип',
        ]);
    }

    /** @test */
    public function itCanGetProductTypes()
    {
        // Создаем тестовый тип данных сразу моделью
        $model = ProductType::create([
           'name' => 'testii',
           'title' => 'Тестовый тип 2',
           'slug' => 'testtype2',
           'product_category_id' => $this->testCategoryId,
        ]);

        // Получаем просто первый попавшийся
        $dto = $this->productTypeService->getFirst();
        $this->assertInstanceOf(ProductTypeDto::class, $dto);

        // Получаем по ID
        $dto2 = $this->productTypeService->getById($model->id);
        $this->assertInstanceOf(ProductTypeDto::class, $dto2);
        $this->assertEquals('testii', $dto2->name);

        // Получаем по name
        $dto3 = $this->productTypeService->getByName('testii');
        $this->assertInstanceOf(ProductTypeDto::class, $dto3);
        $this->assertEquals($model->id, $dto3->id);
    }

    /** @test */
    public function itCanUpdateProductTypes()
    {
        // Создаем тестовый тип данных сразу моделью
        $model = ProductType::create([
            'name' => 'testii',
            'title' => 'Тестовый тип 2',
            'slug' => 'testtype2',
            'product_category_id' => $this->testCategoryId,
        ]);

        // Меняем его сервисом
        $updatedDto = $this->productTypeService->update(
            $model->id,
            name: 'testiii',
            title: 'Тестовый тип 3',
            slug: 'testtype3'
        );

        // Убеждаемся, что получили правильные данные
        $this->assertInstanceOf(ProductTypeDto::class, $updatedDto);
        $this->assertEquals('testiii', $updatedDto->name);
        $this->assertEquals('Тестовый тип 3', $updatedDto->title);
        $this->assertEquals('testtype3', $updatedDto->slug);

        // ..и и что данные действительно поменялись в БД
        $this->assertDatabaseHas('product_types', [
            'id' => $model->id,
            'name' => 'testiii',
            'title' => 'Тестовый тип 3',
        ]);

        // Меняем только одно второстепенное поле, например, стадию
        $updatedDto = $this->productTypeService->update(
            $model->id,
            stage: 'props',
            // Повторяем те же самые значения, чтобы проверить, правильно ли работает валидация
            name: 'testiii',
            title: 'Тестовый тип 3',
            slug: 'testtype3'
        );

        // Убеждаемся, что поменялось
        $this->assertEquals('props', $updatedDto->stage);

        // ... и в базе данных тоже
        $this->assertDatabaseHas('product_types', [
            'id' => $model->id,
            'stage' => 'props',
        ]);
    }

    /** @test */
    public function itDoesntAllowToCreateDuplicateProductTypes()
    {
        // Создаем тестовый тип данных сразу моделью
        $model = ProductType::create([
            'name' => 'testii',
            'title' => 'Тестовый тип 2',
            'slug' => 'testtype2',
            'product_category_id' => $this->testCategoryId,
        ]);

        // Не позволяет дублировать name
        $this->expectException(EntityValidationException::class);
        $this->productTypeService->create(
            name: $model->name,
            title: 'Тестовый тип 3',
            slug: 'testtype3',
            product_category_id: $this->testCategoryId,
        );

        // Не позволяет дублировать name
        $this->expectException(EntityValidationException::class);
        $this->productTypeService->create(
            name: 'name3',
            title: 'Тестовый тип 3',
            slug: $model->slug,
            product_category_id: $this->testCategoryId,
        );
    }

    /** @test */
    public function itCanDeleteProductTypes()
    {
        // Создаем тестовый тип данных сразу моделью
        $model = ProductType::create([
            'name' => 'testii',
            'title' => 'Тестовый тип 2',
            'slug' => 'testtype2',
            'product_category_id' => $this->testCategoryId,
        ]);

        // Убеждаемся, что создалось
        $this->assertDatabaseHas('product_types', ['id' => $model->id]);

        // Удаляем сервисом
        $this->productTypeService->delete($model->id);

        // Убеждаемся что не существует в БД
        $this->assertDatabaseMissing('product_types', ['id' => $model->id]);
    }
}
