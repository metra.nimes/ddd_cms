<?php

namespace Products\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class ProductRouteServiceProvider extends ServiceProvider
{
    public function register()
    {
    }

    public function boot()
    {
        // TODO В целом эти определения и сервис провайдер можно убрать
        Route::pattern('product_category', '\d+');

        // Получаем типы продуктов по их названию
        Route::pattern('product_type', '[a-z\d]{3,10}');

        // Получаем характеристики продуктов в роутах всегда только по ID
        Route::pattern('product_prop', '\d+');
    }
}
