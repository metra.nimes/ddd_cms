<?php

namespace Products\Providers;

use App\Helpers\DomainServiceProvider;

class ProductsServiceProvider extends DomainServiceProvider
{
    protected array $policies  =  [
        'Products\Domain\Models\ProductType'  =>  'Products\Application\Policies\ProductTypePolicy',
        'Products\Domain\Models\ProductCategory'  =>  'Products\Application\Policies\ProductCategoryPolicy',
    ];

    public function register()
    {
        $this->app->register(ProductRouteServiceProvider::class);
    }

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes.php');
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
        $this->registerPolicies();
    }
}
