<?php

namespace Products\Application\Services;

use Products\Application\Http\Resources\ProductPropValueResource;
use Products\Domain\Models\ProductPropValue;
use Products\Domain\Models\ProductType as ProductTypeModel;

class ProductTypeService
{
    public static function getProductTypeProps(string $productType)
    {
        /** @var ProductTypeModel $productType */
        $productType = ProductTypeModel::where('name', $productType)->first();
        $productProps = $productType->getProps();

        // Получаем список значений для отображаемых характеристик
        if (count($productProps)) {
            $productPropValues = ProductPropValue::query()
                ->select('id', 'value', 'title', 'synonyms', 'product_prop_id')
                ->whereIn('product_prop_id', data_get($productProps, '*.id', [0]))
                ->get();
            foreach ($productPropValues as $value) {
                $productProps[$value->product_prop_id]['values'][] = ProductPropValueResource::make($value);
            }
        }

        return $productProps;
    }
}
