<?php

namespace Products\Application\Http\Controllers;

use Accounts\Domain\Services\UserService;
use App\Exceptions\EntityNotCreatedException;
use App\Exceptions\EntityNotDeletedException;
use App\Exceptions\EntityNotFoundException;
use App\Exceptions\EntityNotUpdatedException;
use App\Exceptions\EntityValidationException;
use App\Helpers\NestedSetTree;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Products\Domain\Models\ProductCategory;
use Products\Domain\Services\ProductCategoryService;

class ProductCategoryController extends Controller
{
    public function __construct(
        private ProductCategoryService $productCategoryService,
        private UserService $userService
    ) {
    }

    /**
     * Список товарных категорий
     * GET /product_categories/
     */
    public function index(Request $request)
    {
        // Пока все проверки сводим упрощенно к типам товаров, но дальше было бы полезно специфицировать
        abort_if($request->user()->cannot('viewAny', ProductCategory::class), 403);

        // Получаем имена членов команды, чтобы дальше проставить ответственных
        $teamUserNames = $this->userService->getTeamUserNames();

        $productCategories = $this->productCategoryService->getTree();

        $productCategories = NestedSetTree::traverseTree($productCategories, fn($item) => [
            // Оставляем только нужные поля
            'id' => $item['id'],
            'title' => $item['title'],
            'lead_sales_id' => $item['lead_sales_id'],
            // Дополняем информацией о имени
            'lead_sales_name' => data_get($teamUserNames, $item['lead_sales_id']),
        ]);

        $result = Inertia::render('ProductCategories/Index', [
            'productCategories' => $productCategories,
        ]);

        return $result;
    }

    /**
     * Форма создания товарной категории
     * GET /product_categories/create/
     */
    public function create(Request $request)
    {
        abort_if($request->user()->cannot('create', ProductCategory::class), 403);

        $teamUserNames = $this->userService->getTeamUserNames();

        return Inertia::render('ProductCategories/Create', [
            'teamUserNames' => $teamUserNames,
        ]);
    }

    /**
     * Сохранение созданной товарной категории
     * POST /product_categories/
     */
    public function store(Request $request)
    {
        abort_if($request->user()->cannot('create', ProductCategory::class), 403);

        try {
            $this->productCategoryService->create(
                title: $request->stringOrNull('title'),
                lead_sales_id: $request->intOrNull('lead_sales_id'),
            );
        } catch (EntityValidationException $exception) {
            return back()->withErrors($exception->messages);
        } catch (EntityNotCreatedException $exception) {
            // Пока что просто выводим ошибку под любым полем, чтобы было видно
            // TODO В будущем нужно логировать такие ошибки, и выводить системную ошибку.
            return back()->withErrors(['name' => 'Не удается создать категорию: ' . $exception->getMessage()]);
        }

        return Redirect::route('product_categories.index');
    }

    /**
     * Форма редактирования товарной категории
     * GET /product_categories/{product_category}/edit/
     */
    public function edit(int $productCategory, Request $request)
    {
        try {
            $productCategoryDto = $this->productCategoryService->getById($productCategory);
            abort_if($request->user()->cannot('update', $productCategoryDto), 403);
            $teamUserNames = $this->userService->getTeamUserNames();
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        return Inertia::render('ProductCategories/Edit', [
            'id' => $productCategoryDto->id,
            'values' => $productCategoryDto,
            'teamUserNames' => $teamUserNames,
        ]);
    }

    /**
     * Сохранение редактируемой товарной категории
     * PUT /product_categories/{product_category}/
     */
    public function update(int $productCategory, Request $request)
    {
        try {
            $productCategoryDto = $this->productCategoryService->getById($productCategory);
            abort_if($request->user()->cannot('update', $productCategoryDto), 403);
            $this->productCategoryService->update(
                id: $productCategoryDto->id,
                title: $request->stringOrNull('title'),
                lead_sales_id: $request->intOrNull('lead_sales_id'),
            );
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        } catch (EntityValidationException $exception) {
            return back()->withErrors($exception->messages);
        } catch (EntityNotUpdatedException $exception) {
            // Пока что просто выводим ошибку под любым полем, чтобы было видно
            // TODO В будущем нужно логировать такие ошибки, и выводить системную ошибку.
            return back()->withErrors(['name' => 'Не удается отредактировать категорию: ' . $exception->message]);
        }

        return Redirect::route('product_categories.index');
    }

    /**
     * Перемещение товарной категории
     * PUT /product_categories/{product_category}/
     */
    public function move(int $productCategory, Request $request)
    {
        try {
            $productCategoryDto = $this->productCategoryService->getById($productCategory);
            abort_if($request->user()->cannot('update', $productCategoryDto), 403);
            $this->productCategoryService->move(
                id: $productCategory,
                target: $request->integer('target'),
                relation: $request->string('relation')
            );
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        } catch (EntityNotUpdatedException $exception) {
            // Пока что просто выводим ошибку под любым полем, чтобы было видно
            // TODO В будущем нужно логировать такие ошибки, и выводить системную ошибку.
            return back()->withErrors(['name' => 'Не удается переместить категорию: ' . $exception->message]);
        }

        return Redirect::back(303);
    }

    /**
     * Удаление товарной категории
     * DELETE /product_categories/{product_category}/
     */
    public function destroy(int $productCategory, Request $request)
    {
        try {
            $productCategoryDto = $this->productCategoryService->getById($productCategory);
            abort_if($request->user()->cannot('delete', $productCategoryDto), 403);
            $this->productCategoryService->delete($productCategoryDto->id);
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        } catch (EntityNotDeletedException $exception) {
            // TODO В будущем нужно логировать такие ошибки, и выводить системную ошибку.
            back()->withErrors(['id' => $exception->message]);
        }

        return Redirect::back(303);
    }
}
