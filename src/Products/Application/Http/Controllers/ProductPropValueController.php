<?php

namespace Products\Application\Http\Controllers;

use App\Exceptions\EntityNotCreatedException;
use App\Exceptions\EntityNotDeletedException;
use App\Exceptions\EntityNotFoundException;
use App\Exceptions\EntityNotUpdatedException;
use App\Exceptions\EntityValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Products\Domain\Services\ProductPropService;
use Products\Domain\Services\ProductPropValueService;
use Products\Domain\Services\ProductTypeService;

class ProductPropValueController extends Controller
{
    public function __construct(
        private ProductTypeService $productTypeService,
        private ProductPropService $productPropService,
        private ProductPropValueService $productPropValueService
    ) {
    }

    /**
     * Создать значение характеристики
     *
     * POST /product_prop_values
     */
    public function store(Request $request)
    {
        try {
            $productPropDto = $this->productPropService->getById($request->integer('product_prop_id'));
            $productTypeDto = $this->productTypeService->getById($productPropDto->product_type_id);
            abort_if($request->user()->cannot('update', $productTypeDto), 403);
            $this->productPropValueService->create(
                productPropId: $productPropDto->id,
                value: $request->stringOrNull('value'),
                title: $request->stringOrNull('title'),
                synonyms: $request->arrayOrNull('synonyms')
            );
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        } catch (EntityValidationException $exception) {
            return back()->withErrors($exception->messages);
        } catch (EntityNotCreatedException $exception) {
            // Пока что просто выводим ошибку под любым полем, чтобы было видно
            // TODO В будущем нужно логировать такие ошибки, и выводить системную ошибку.
            return back()->withErrors(['value' => 'Не удается создать значение: ' . $exception->getMessage()]);
        }

        // TODO Отправлять события
        //Amqp::publish('events:product_type.updated.props', [
        //    'id' => $productType->id,
        //    'attr' => 'values',
        //    'new_value' => $productType->getProps(),
        //    'updated_at' => $productProp->updated_at,
        //]);

        return Redirect::back(303);
    }

    /**
     * Сохранить изменения в значение характеристики
     *
     * PUT /product_prop_values/{id}
     */
    public function update(Request $request, int $id)
    {
        try {
            $productPropValue = $this->productPropValueService->getById($id);
            $productPropDto = $this->productPropService->getById($productPropValue->product_prop_id);
            $productTypeDto = $this->productTypeService->getById($productPropDto->product_type_id);
            abort_if($request->user()->cannot('update', $productTypeDto), 403);
            $this->productPropValueService->update(
                id: $id,
                value: $request->stringOrNull('value'),
                title: $request->stringOrNull('title'),
                synonyms: $request->arrayOrNull('synonyms')
            );
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        } catch (EntityValidationException $exception) {
            return back()->withErrors($exception->messages);
        } catch (EntityNotUpdatedException $exception) {
            // Пока что просто выводим ошибку под любым полем, чтобы было видно
            // TODO В будущем нужно логировать такие ошибки, и выводить системную ошибку.
            return back()->withErrors(['value' => 'Не удается отредактировать значение: ' . $exception->message]);
        }

        // TODO Отправлять события
        //Amqp::publish('events:product_type.updated.props', [
        //    'id' => $productType->id,
        //    'attr' => 'values',
        //    'new_value' => $productType->getProps(),
        //    'updated_at' => $productProp->updated_at,
        //]);

        return Redirect::back(303);
    }

    /**
     * Удалить значение характеристики
     *
     * DELETE /product_prop_values/{id}
     */
    public function destroy(Request $request, int $id)
    {
        try {
            $productPropValue = $this->productPropValueService->getById($id);
            $productPropDto = $this->productPropService->getById($productPropValue->product_prop_id);
            $productTypeDto = $this->productTypeService->getById($productPropDto->product_type_id);
            abort_if($request->user()->cannot('update', $productTypeDto), 403);
            $this->productPropValueService->delete($productPropValue->id);
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        } catch (EntityNotDeletedException $exception) {
            // TODO В будущем нужно логировать такие ошибки, и выводить системную ошибку.
            back()->withErrors(['id' => $exception->message]);
        }

        // TODO Отправить событие
        //Amqp::publish('events:product_type.updated.props', [
        //    'id' => $productType->id,
        //    'attr' => 'values',
        //    'new_value' => $productType->getProps(),
        //    'updated_at' => $productProp->updated_at,
        //]);

        return Redirect::back(303);
    }
}
