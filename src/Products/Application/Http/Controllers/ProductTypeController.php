<?php

namespace Products\Application\Http\Controllers;

use Accounts\Domain\Services\UserService;
use App\Exceptions\EntityNotCreatedException;
use App\Exceptions\EntityNotDeletedException;
use App\Exceptions\EntityNotFoundException;
use App\Exceptions\EntityNotUpdatedException;
use App\Exceptions\EntityValidationException;
use App\Helpers\DomainModelController;
use App\Helpers\NestedSetTree;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Products\Contracts\DataTransferObjects\ProductTypeDto;
use Products\Domain\Models\ProductType;
use Products\Domain\Services\ProductCategoryService;
use Products\Domain\Services\ProductTypeService;

class ProductTypeController extends Controller
{
    use DomainModelController;

    public function __construct(
        private ProductTypeService $productTypeService,
        private ProductCategoryService $productCategoryService,
        private UserService $userService
    ) {
    }

    /**
     * Список типов товаров
     * GET /product_types/
     */
    public function index(Request $request)
    {
        abort_if($request->user()->cannot('viewAny', ProductType::class), 403);

        $paginatedProductTypes = $this->productTypeService->list(
            stage: $request->get('stage'),
            searchQuery: $request->get('q'),
            category: $request->get('category'),
            orderBy: 'title',
            linksQueryString: $request->except(['page'])
        );

        // Получаем имена членов команды, чтобы дальше проставить ответственных
        $teamUserNames = $this->userService->getTeamUserNames();

        // Получаем в виде дерева, чтобы подставить в фильтр
        $categoryTree = $this->productCategoryService->getTree();

        // Получаем просто список id => заголовок, из существующих данных, чтобы не делать доп.запрос к БД
        $categoriesTitles = [];
        NestedSetTree::traverseTree($categoryTree, function ($item) use (&$categoriesTitles) {
            $categoriesTitles[$item['id']] = $item['title'];
        });

        $publishedStages = config('models.product_types.published_stages', []);

        return Inertia::render('ProductTypes/Index', [
            'productTypes' => $this->outputPaginatedList($paginatedProductTypes, fn(ProductTypeDto $productType) => [
                'id' => $productType->id,
                'title' => $productType->title,
                'stage' => $productType->stage,
                'name' => $productType->name,
                'slug' => $productType->slug,
                'productCategoryTitle' => data_get($categoriesTitles, $productType->product_category_id),
                'isPublished' => in_array($productType->stage, $publishedStages),
                'leadContentId' => $productType->lead_content_id,
                'leadContentName' => data_get($teamUserNames, $productType->lead_content_id ?? 0),
            ]),
            'initialFilter' => [
                // Пробрасываем именно числом, т.к. идет строгое сравнение
                'category' => isset($request->category) ? (int)$request->category : null,
                'stage' => $request->stage,
                'q' => $request->q,
            ],
            'stageTitles' => array_map(fn($item) => $item['title'], config('models.product_types.stages', [])),
            'categoryTree' => $categoryTree,
        ]);
    }

    /**
     * Форма создания типа товара
     * GET /product_types/create/
     */
    public function create(Request $request)
    {
        abort_if($request->user()->cannot('create', ProductType::class), 403);

        $categoryTree = $this->productCategoryService->getTree();

        $teamUserNames = $this->userService->getTeamUserNames();

        return Inertia::render('ProductTypes/Create', [
            'categoryTree' => $categoryTree,
            'stages' => collect(config('models.product_types.stages', []))->map(fn($item) => $item['title'])->toArray(),
            'publishedStages' => config('models.product_types.published_stages', []),
            'teamUserNames' => $teamUserNames,
        ]);
    }

    /**
     * Сохранение созданного типа товара
     * POST /product_types/
     */
    public function store(Request $request)
    {
        abort_if($request->user()->cannot('create', ProductType::class), 403);

        try {
            $this->productTypeService->create(
                name: $request->stringOrNull('name'),
                title: $request->stringOrNull('title'),
                slug: $request->stringOrNull('slug'),
                product_category_id: $request->intOrNull('product_category_id'),
                publish_unrecognized_offers: $request->boolOrNull('publish_unrecognized_offers'),
                lead_content_id: $request->intOrNull('lead_content_id'),
                lead_developer_id: $request->intOrNull('lead_developer_id'),
            );
        } catch (EntityValidationException $exception) {
            return back()->withErrors($exception->messages);
        } catch (EntityNotCreatedException $exception) {
            // Пока что просто выводим ошибку под любым полем, чтобы было видно
            // TODO В будущем нужно логировать такие ошибки, и выводить системную ошибку.
            return back()->withErrors(['name' => 'Не удается создать тип товара: ' . $exception->getMessage()]);
        }

        return Redirect::route('product_types.index');
    }

    /**
     * Форма редактирования типа товара
     * GET /product_types/{product_type}/edit/
     */
    public function edit(string $productType, Request $request)
    {
        try {
            $productTypeDto = $this->productTypeService->getByName($productType);
            abort_if($request->user()->cannot('update', $productTypeDto), 403);
            // Получаем дерево категорий для подстановки в выпадающий список
            $categoryTree = $this->productCategoryService->getTree();
            $teamUserNames = $this->userService->getTeamUserNames();
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        return Inertia::render('ProductTypes/Edit', [
            'name' => $productTypeDto->name,
            'values' => $productTypeDto,
            'categoryTree' => $categoryTree,
            'stages' => collect(config('models.product_types.stages', []))->map(fn($item) => $item['title'])->toArray(),
            'publishedStages' => config('models.product_types.published_stages', []),
            'teamUserNames' => $teamUserNames,
        ]);
    }

    /**
     * Сохранение редактируемого типа товара
     * PUT /product_types/{product_type}/
     */
    public function update(string $productType, Request $request)
    {
        try {
            $productTypeDto = $this->productTypeService->getByName($productType);
            abort_if($request->user()->cannot('update', $productTypeDto), 403);
            $this->productTypeService->update(
                id: $productTypeDto->id,
                name: $request->stringOrNull('name'),
                title: $request->stringOrNull('title'),
                slug: $request->stringOrNull('slug'),
                product_category_id: $request->intOrNull('product_category_id'),
                synonyms: $request->arrayOrNull('synonyms'),
                stage: $request->stringOrNull('stage'),
                publish_unrecognized_offers: $request->boolOrNull('publish_unrecognized_offers'),
                lead_content_id: $request->intOrNull('lead_content_id'),
                lead_developer_id: $request->intOrNull('lead_developer_id'),
            );
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        } catch (EntityValidationException $exception) {
            return back()->withErrors($exception->messages);
        } catch (EntityNotUpdatedException $exception) {
            // Пока что просто выводим ошибку под любым полем, чтобы было видно
            // TODO В будущем нужно логировать такие ошибки, и выводить системную ошибку.
            return back()->withErrors(['name' => 'Не удается отредактировать тип товара: ' . $exception->message]);
        }

        return Redirect::route('product_types.index');
    }

    /**
     * Удаление типа товара
     * DELETE /product_types/{product_type}/
     */
    public function destroy(string $productType, Request $request)
    {
        try {
            $productTypeDto = $this->productTypeService->getByName($productType);
            abort_if($request->user()->cannot('delete', $productTypeDto), 403);
            $this->productTypeService->delete($productTypeDto->id);
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        } catch (EntityNotDeletedException $exception) {
            // TODO В будущем нужно логировать такие ошибки, и выводить системную ошибку.
            back()->withErrors(['name' => $exception->message]);
        }

        return Redirect::back(303);
    }
}
