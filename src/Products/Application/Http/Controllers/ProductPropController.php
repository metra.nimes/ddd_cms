<?php

namespace Products\Application\Http\Controllers;

use Amqp;
use App\Exceptions\EntityNotCreatedException;
use App\Exceptions\EntityNotDeletedException;
use App\Exceptions\EntityNotFoundException;
use App\Exceptions\EntityNotUpdatedException;
use App\Exceptions\EntityValidationException;
use App\Helpers\DomainModelController;
use App\Helpers\Navigation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Products\Contracts\DataTransferObjects\ProductPropDto;
use Products\Contracts\DataTransferObjects\ProductTypeDto;
use Products\Domain\Services\ProductPropService;
use Products\Domain\Services\ProductPropValueService;
use Products\Domain\Services\ProductTypeService;

class ProductPropController extends Controller
{
    use DomainModelController;

    public function __construct(
        private ProductTypeService $productTypeService,
        private ProductPropService $productPropService,
        private ProductPropValueService $productPropValueService
    ) {
    }

    /**
     * Список характеристик товаров
     * GET /product_types/{product_type}/product_props
     */
    public function index(string $productType, Request $request)
    {
        try {
            $productTypeDto = $this->productTypeService->getByName($productType);
            abort_if($request->user()->cannot('view', $productTypeDto), 403);

            $this->addProductTypeSwitcher();

            $props = $this->productPropService->list(
                productTypeId: $productTypeDto->id,
                searchQuery: $request->get('q'),
                isPricelistProp: $request->get('pricelist'),
                isModelProp: $request->get('model'),
                isFilterProp: $request->get('filter')
            );
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        // Упорядочиваем формат, и подставляем ID-ключи, чтобы дальше проставлять значения
        $productProps = collect($props)
            ->map(fn(ProductPropDto $prop) => [
                // Не выносим в ресурс, т.к. уникальное представление для этого view
                'id' => $prop->id,
                'title' => $prop->title,
                'slug' => $prop->slug,
                'is_pricelist_prop' => $prop->is_pricelist_prop,
                'is_model_prop' => $prop->is_model_prop,
                'is_filter_prop' => $prop->is_filter_prop,
                'values' => [],
            ])
            ->keyBy('id')
            ->toArray();

        // Получаем список значений для отображаемых характеристик
        if (count($productProps)) {
            $productPropValues = $this->productPropValueService->listPropsValues(array_keys($productProps));
            foreach ($productPropValues as $propId => $propValues) {
                $productProps[$propId]['values'] = $propValues;
            }
        }

        return Inertia::render('ProductProps/Index', [
            'productProps' => array_values($productProps),
            'productType' => $productTypeDto->name,
            'initialFilter' => $request->only(['pricelist', 'model', 'filter', 'q']),
        ]);
    }

    /**
     * Форма создания характеристики товара
     * GET /product_types/{product_type}/product_props/create
     */
    public function create(string $productType, Request $request)
    {
        try {
            $productTypeDto = $this->productTypeService->getByName($productType);
            abort_if($request->user()->cannot('update', $productTypeDto), 403);
            $this->addProductTypeSwitcher();
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        return Inertia::render('ProductProps/Create', [
            'productTypeName' => $productTypeDto->name,
            'productTypeTitle' => $productTypeDto->title,
            'productPropTypes' => config('models.product_props.types', []),
            'decimalNumbersOptions' => config('models.product_props.decimal_numbers', []),
            'filterOptionsLayouts' => config('models.product_props.filter_options_layouts', []),
            'filterOptionsOrders' => config('models.product_props.filter_options_orders', []),
        ]);
    }

    /**
     * Сохранение созданной характеристики
     * POST /product_types/{product_type}/product_props
     */
    public function store(string $productType, Request $request)
    {
        try {
            $productTypeDto = $this->productTypeService->getByName($productType);
            abort_if($request->user()->cannot('update', $productTypeDto), 403);
            $this->productPropService->create(
                product_type_id: $productTypeDto->id,
                title: $request->stringOrNull('title'),
                slug: $request->stringOrNull('slug'),
                type: $request->stringOrNull('type'),
                is_multiple: $request->boolOrNull('is_multiple'),
                decimal_numbers: $request->intOrNull('decimal_numbers'),
                validation_regexp: $request->stringOrNull('validation_regexp'),
                min_value: $request->floatOrNull('min_value'),
                max_value: $request->floatOrNull('max_value'),
                is_pricelist_prop: $request->boolOrNull('is_pricelist_prop'),
                synonyms: $request->arrayOrNull('synonyms'),
                required_for_offer: $request->boolOrNull('required_for_offer'),
                is_model_prop: $request->boolOrNull('is_model_prop'),
                required_for_model: $request->boolOrNull('required_for_model'),
                is_filter_prop: $request->boolOrNull('is_filter_prop'),
                is_parent_tag: $request->boolOrNull('is_parent_tag'),
                filter_title: $request->stringOrNull('filter_title'),
                filter_options_url: $request->stringOrNull('filter_options_url'),
                filter_options_layout: $request->stringOrNull('filter_options_layout'),
                filter_has_search: $request->boolOrNull('filter_has_search'),
                filter_options_order: $request->stringOrNull('filter_options_order'),
                filter_hint: $request->stringOrNull('filter_hint')
            );
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        } catch (EntityValidationException $exception) {
            return back()->withErrors($exception->messages);
        } catch (EntityNotCreatedException $exception) {
            // Пока что просто выводим ошибку под любым полем, чтобы было видно
            // TODO В будущем нужно логировать такие ошибки, и выводить системную ошибку.
            return back()->withErrors([
                'slug' => 'Не удается создать характеристику товара: ' . $exception->getMessage()
            ]);
        }

        return Redirect::route('product_types.product_props.index', ['product_type' => $productTypeDto->name]);
    }

    /**
     * Форма редактирования характеристики товара
     * GET /product_props/{product_prop}/edit
     */
    public function edit(Request $request, int $productProp)
    {
        try {
            $productPropDto = $this->productPropService->getById($productProp);
            $productTypeDto = $this->productTypeService->getById($productPropDto->product_type_id);
            abort_if($request->user()->cannot('update', $productTypeDto), 403);
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        }

        return Inertia::render('ProductProps/Edit', [
            'id' => $productPropDto->id,
            'values' => $productPropDto,
            'productTypeName' => $productTypeDto->name,
            'productTypeTitle' => $productTypeDto->title,
            'productPropTypes' => config('models.product_props.types', []),
            'decimalNumbersOptions' => config('models.product_props.decimal_numbers', []),
            'filterOptionsLayouts' => config('models.product_props.filter_options_layouts', []),
            'filterOptionsOrders' => config('models.product_props.filter_options_orders', []),
        ]);
    }

    /**
     * Сохранение характеристики товара
     * PUT /product_props/{product_prop}
     */
    public function update(int $productProp, Request $request)
    {
        try {
            $productPropDto = $this->productPropService->getById($productProp);
            $productTypeDto = $this->productTypeService->getById($productPropDto->product_type_id);
            abort_if($request->user()->cannot('update', $productTypeDto), 403);
            $this->productPropService->update(
                id: $productPropDto->id,
                title: $request->stringOrNull('title'),
                slug: $request->stringOrNull('slug'),
                type: $request->stringOrNull('type'),
                is_multiple: $request->boolOrNull('is_multiple'),
                decimal_numbers: $request->intOrNull('decimal_numbers'),
                validation_regexp: $request->stringOrNull('validation_regexp'),
                min_value: $request->floatOrNull('min_value'),
                max_value: $request->floatOrNull('max_value'),
                is_pricelist_prop: $request->boolOrNull('is_pricelist_prop'),
                synonyms: $request->arrayOrNull('synonyms'),
                required_for_offer: $request->boolOrNull('required_for_offer'),
                is_model_prop: $request->boolOrNull('is_model_prop'),
                required_for_model: $request->boolOrNull('required_for_model'),
                is_filter_prop: $request->boolOrNull('is_filter_prop'),
                is_parent_tag: $request->boolOrNull('is_parent_tag'),
                filter_title: $request->stringOrNull('filter_title'),
                filter_options_url: $request->stringOrNull('filter_options_url'),
                filter_options_layout: $request->stringOrNull('filter_options_layout'),
                filter_has_search: $request->boolOrNull('filter_has_search'),
                filter_options_order: $request->stringOrNull('filter_options_order'),
                filter_hint: $request->stringOrNull('filter_hint')
            );
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        } catch (EntityValidationException $exception) {
            return back()->withErrors($exception->messages);
        } catch (EntityNotUpdatedException $exception) {
            // Пока что просто выводим ошибку под любым полем, чтобы было видно
            // TODO В будущем нужно логировать такие ошибки, и выводить системную ошибку.
            return back()->withErrors(['name' => 'Не удается отредактировать характеристику: ' . $exception->message]);
        }

        return Redirect::route('product_types.product_props.index', ['product_type' => $productTypeDto->name]);
    }

    /**
     * Удаление характеристики товара
     * DELETE /product_props/{product_prop}
     */
    public function destroy(int $productProp, Request $request)
    {
        try {
            $productPropDto = $this->productPropService->getById($productProp);
            $productTypeDto = $this->productTypeService->getById($productPropDto->product_type_id);
            abort_if($request->user()->cannot('update', $productTypeDto), 403);
            $this->productPropService->delete($productPropDto->id);
        } catch (EntityNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND);
        } catch (EntityNotDeletedException $exception) {
            // TODO В будущем нужно логировать такие ошибки, и выводить системную ошибку.
            back()->withErrors(['id' => $exception->message]);
        }

        return Redirect::back(303);
    }

    /**
     * Добавить переключатель типа товара в шапку
     * @return void
     */
    protected function addProductTypeSwitcher()
    {
        $productTypes = collect($this->productTypeService->list(perPage: 0)->data)
            ->keyBy('name')
            ->map(fn(ProductTypeDto $pType) => $pType->title)
            ->toArray();

        Navigation::addSwitcher('product_type', $productTypes, 'Выберите тип товара');
    }
}
