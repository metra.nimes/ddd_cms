<?php

namespace Products\Application\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Products\Application\Http\Resources\ProductTypeResource;
use Products\Application\Services\ProductTypeService;
use Products\Domain\Models\ProductType as ProductTypeModel;

class ProductTypeController extends Controller
{
    /**
     * Получить список типов продуктов
     * AMQP requests:product_type.list
     */
    public function list(): JsonResponse
    {
        return response()->json(ProductTypeResource::collection(
            ProductTypeModel::all()
        ));
    }

    /**
     * Получить характеристики и их значения для указанного типа продукта
     * AMQP requests:product_type.export_props
     */
    public function exportProps(Request $request): JsonResponse
    {
        $productTypeName = $request->get('name');
        abort_if(! $productTypeName, 400, 'Product type is required');

        $productProps = ProductTypeService::getProductTypeProps($productTypeName);

        return response()->json($productProps);
    }

    /**
     * Тестовый метод для зполнения vendor_products на парсерах
     * AMQP requests:product_type.export_vendors_products
     *
     * TODO: удалить тестовый метод
     */
    public function exportVendorsProducts(Request $request): JsonResponse
    {
        $product_type = $request->get('product_type');
        //abort_if($product_type !== 'tire', 400, 'Product type is not allowed');

        $vendors_products = file_get_contents(base_path('database/seeders/export_vendor_products.json'));
        $vendors_products = json_decode($vendors_products, true);

        return response()->json(
            Arr::get($vendors_products, $product_type, [])
        );
    }
}
