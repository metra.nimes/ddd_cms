<?php

namespace Products\Application\Http\Resources;

use Accounts\Domain\Services\UserService;
use Illuminate\Http\Resources\Json\JsonResource;
use Products\Domain\Models\ProductCategory;

// TODO Удалить этот ресурс, заменить наборами ключей на местах
class ProductTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // TODO Вот здесь прямого вызова к моделям не может быть — нужно выносить на надуровень
        $teamUsersNames = (new UserService())->getTeamUserNames();
        static $productCategories = null;
        if ($productCategories === null) {
            // @phpstan-ignore-next-line
            $productCategories = ProductCategory::all()->toFlatTree()->pluck('title', 'id')->toArray();
        }

        $publishedStages = [];
        return [
            'id' => $this->id,
            'title' => $this->title,
            'stage' => $this->stage,
            'name' => $this->name,
            'slug' => $this->slug,
            $this->mergeWhen($request->route()?->getName() === 'product_types.index', [
                'productCategoryTitle' => data_get(
                    $productCategories,
                    $this->product_category_id,
                    $this->product_category_id
                ),
                'isPublished' => in_array($this->stage, $publishedStages),
                'leadContentId' => $this->lead_content_id,
                'leadContentName' => data_get($teamUsersNames, $this->lead_content_id ?? 0),
            ]),

            $this->mergeWhen($request->route()?->getName() !== 'product_types.index', [
                'synonyms' => $this->synonyms,
                'keywords' => $this->keywords,
                'publish_unrecognized_offers' => (int) $this->publish_unrecognized_offers,
            ]),

        ];
    }
}
