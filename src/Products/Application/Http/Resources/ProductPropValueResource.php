<?php

namespace Products\Application\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductPropValueResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'value' => $this->value,
            'title' => $this->title,
            'synonyms' => $this->synonyms,
        ];
    }
}
