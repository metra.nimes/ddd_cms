<?php

namespace Products\Application\Policies;

use Accounts\Domain\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Products\Contracts\DataTransferObjects\ProductTypeDto;
use Products\Domain\Models\ProductType;

class ProductTypePolicy
{
    use HandlesAuthorization;

    /**
     * Может ли пользователь просматривать список типов продуктов в целом
     */
    public function viewAny(User $user)
    {
        // Просматривать могут все участники команды
        return in_array($user->role, config('models.users.team_roles', ['admin']), true);
    }

    /**
     * Может ли пользователь просматривать конкретный тип товара (характеристики и тп)
     */
    public function view(User $user, ProductType|ProductTypeDto $productType)
    {
        // Могут просмтаривать админы, и контент-менеджеры, прикрепленные к типу товара
        // TODO Прикрепление нескольких контент-менеджеров к одному типу товара
        return ($user->role === 'admin' or $productType->lead_content_id === $user->id
            or $productType->lead_developer_id === $user->id);
    }

    /**
     * Может ли пользователь создавать новые типы товара
     */
    public function create(User $user)
    {
        // Могут только админы
        return ($user->role === 'admin');
    }

    /**
     * Может ли пользователь редактировать тип товара (в том числе характеристики)
     */
    public function update(User $user, ProductType|ProductTypeDto $productType)
    {
        // Могут редактировать админы, ведущий контент-менеджер и программист
        return ($user->role === 'admin' or $productType->lead_content_id === $user->id
            or $productType->lead_developer_id === $user->id);
    }

    /**
     * Может ли пользователь удалять тип товара
     */
    public function delete(User $user, ProductType|ProductTypeDto $productType)
    {
        // Могут только админы и то, только если тип товара не опубликован
        return ($user->role === 'admin');
    }
}
