<?php

namespace Products\Application\Policies;

use Accounts\Domain\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Products\Contracts\DataTransferObjects\ProductCategoryDto;
use Products\Domain\Models\ProductCategory;

class ProductCategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Может ли пользователь просматривать список категорий товаров в целом
     */
    public function viewAny(User $user)
    {
        // Могут все участники команды
        return in_array($user->role, config('models.users.team_roles', ['admin']), true);
    }

    /**
     * Может ли пользователь просматривать конкретную категорию товара
     */
    public function view(User $user, ProductCategory|ProductCategoryDto $productCategory)
    {
        // Могут все участники команды
        return in_array($user->role, config('models.users.team_roles', ['admin']), true);
    }

    /**
     * Может ли пользователь создавать новые категории товаров
     */
    public function create(User $user)
    {
        // Могут только админы
        return ($user->role === 'admin');
    }

    /**
     * Может ли пользователь редактировать категорию товара
     */
    public function update(User $user, ProductCategory|ProductCategoryDto $productCategory)
    {
        // Могут только админы
        return ($user->role === 'admin');
    }

    /**
     * Может ли пользователь удалять тип товара
     */
    public function delete(User $user, ProductCategory|ProductCategoryDto $productCategory)
    {
        // Могут только админы
        return ($user->role === 'admin');
    }
}
