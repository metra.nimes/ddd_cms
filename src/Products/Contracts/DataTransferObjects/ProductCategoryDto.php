<?php

namespace Products\Contracts\DataTransferObjects;

class ProductCategoryDto
{
    public function __construct(
        public readonly ?int $id,
        public readonly ?string $title,
        public readonly ?int $lead_sales_id = null,
        public readonly ?string $created_at = null,
        public readonly ?string $updated_at = null
    ) {
    }
}
