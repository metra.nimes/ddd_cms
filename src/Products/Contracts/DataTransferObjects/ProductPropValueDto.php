<?php

namespace Products\Contracts\DataTransferObjects;

class ProductPropValueDto
{
    public function __construct(
        public readonly ?int $id,
        public readonly ?int $product_prop_id,
        public readonly ?string $value,
        public readonly ?string $title = null,
        public readonly ?array $synonyms = null,
        public readonly ?bool $is_visible = true
    ) {
    }
}
