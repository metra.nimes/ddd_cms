<?php

namespace Products\Contracts\DataTransferObjects;

class ProductTypeDto
{
    public function __construct(
        public readonly int $id,
        public readonly string $name,
        public readonly string $title,
        public readonly string $slug,
        public readonly int $product_category_id,
        public readonly ?string $stage = null,
        public readonly ?array $synonyms = [],
        public readonly ?bool $publish_unrecognized_offers = null,
        public readonly ?int $lead_content_id = null,
        public readonly ?int $lead_developer_id = null,
        public readonly ?string $created_at = null,
        public readonly ?string $updated_at = null
    ) {
    }
}
