<?php

namespace Products\Contracts\DataTransferObjects;

class ProductPropDto
{
    public function __construct(
        public readonly ?int $id,
        public readonly ?int $product_type_id,
        public readonly ?string $title,
        /**
         * @var ?string Название шорткода для подстановки значений и GET-параметра в фильтрах
         */
        public readonly ?string $slug,
        /**
         * @var ?string Значения из config/models:product_props.types: string, enum, int, decimal, bool
         */
        public readonly ?string $type,
        /**
         * @var ?bool Для enum-типа: может ли товар иметь несколько значений сразу
         */
        public readonly ?bool $is_multiple = null,
        /**
         * @var ?int Для decimal-типа: кол-во десятичных знаков
         */
        public readonly ?int $decimal_numbers = null,
        /**
         * @var ?string Для string-типа: регулярное выражение для валидации
         */
        public readonly ?string $validation_regexp = null,
        /**
         * @var ?float Для int/decimal-типов: минимальное допустимое значение
         */
        public readonly ?float $min_value = null,
        /**
         * @var ?float Для int/decimal-типов: максимальное допустимое значение
         */
        public readonly ?float $max_value = null,
        /**
         * @var ?bool Возможно ли получить значение из прайс-листа?
         */
        public readonly ?bool $is_pricelist_prop = null,
        /**
         * @var ?array Синонимы обозначения характеристики в прайс-листе
         */
        public readonly ?array $synonyms = null,
        /**
         * @var ?bool Должно ли значение быть распознано в прайс-листе и валидно, чтобы предложение
         *     считалось правильно распознанным?
         */
        public readonly ?bool $required_for_offer = null,
        /**
         * @var ?bool Является ли характеристикой модели? (Т.е. для всех предложений модели оно будет
         *     одинаковым.)
         */
        public readonly ?bool $is_model_prop = null,
        /**
         * @var ?bool Должно ли значение быть заполнено и валидно, чтобы карточку товара можно было
         *     опубликовать?
         */
        public readonly ?bool $required_for_model = null,
        /**
         * @var ?bool Выводить ли фильтр по характеристике?
         */
        public readonly ?bool $is_filter_prop = null,
        /**
         * @var ?bool Может ли тег по характеристике содержать вложенные теги?
         */
        public readonly ?bool $is_parent_tag = null,
        /**
         * @var ?string Название фильтра
         */
        public readonly ?string $filter_title = null,
        /**
         * @var ?string Если значения не относятся ни к предложению, ни к товару, то с какого URL
         *     получать список значений?
         */
        public readonly ?string $filter_options_url = null,
        /**
         * @var ?string Способ отображения вариантов фильтра: checkboxes, dropdown (значения из
         *     config/models:product_props:filter_options_layouts)
         */
        public readonly ?string $filter_options_layout = null,
        /**
         * @var ?bool Выводить ли в фильтре строку поиска по вариантам?
         */
        public readonly ?bool $filter_has_search = null,
        /**
         * @var ?string Вид сортировки вариантов фильтра: alphabet, number (значение из
         *     config/models:filter_options_orders)
         */
        public readonly ?string $filter_options_order = null,
        /**
         * @var ?string Подсказка к фильтру
         */
        public readonly ?string $filter_hint = null,
        public readonly ?string $created_at = null,
        public readonly ?string $updated_at = null
    ) {
    }
}
