<?php

declare(strict_types=1);

namespace Products\Contracts;

use App\Contracts\DataTransferObjects\PaginatedListDto;
use Products\Contracts\DataTransferObjects\ProductTypeDto;

interface ProductTypeServiceContract
{
    /**
     * Получить тип товара по ID
     *
     * @param int $id
     *
     * @return ProductTypeDto
     *
     * @throws \App\Exceptions\EntityNotFoundException
     */
    public function getById(int $id): ProductTypeDto;

    /**
     * Получить тип товара по системному названию
     *
     * @param string $name
     *
     * @return ProductTypeDto
     *
     * @throws \App\Exceptions\EntityNotFoundException
     */
    public function getByName(string $name): ProductTypeDto;

    /**
     * Получить первый (самый приоритетный) тип товара
     *
     * @return ProductTypeDto
     *
     * @throws \App\Exceptions\EntityNotFoundException
     */
    public function getFirst(): ProductTypeDto;

    /**
     * Получить список типов товаров
     *
     * @param ?string $stage
     * @param ?string $searchQuery
     * @param ?string $category
     * @param ?string $orderBy
     * @param int $perPage
     * @param array $linksQueryString Дополнительные GET-параметры, которые нужно добавить к ссылкам
     *
     * @return PaginatedListDto
     */
    public function list(
        ?string $stage = null,
        ?string $searchQuery = null,
        ?string $category = null,
        ?string $orderBy = null,
        int $perPage = 25,
        array $linksQueryString = []
    ): PaginatedListDto;

    /**
     * Создать тип товара
     *
     * @param ?string $name
     * @param ?string $title
     * @param ?string $slug
     * @param ?int $product_category_id
     * @param ?bool $publish_unrecognized_offers
     * @param ?int $lead_content_id
     * @param ?int $lead_developer_id
     *
     * @return ProductTypeDto
     *
     * @throws \App\Exceptions\EntityValidationException
     * @throws \App\Exceptions\EntityNotCreatedException
     */
    public function create(
        ?string $name,
        ?string $title,
        ?string $slug,
        ?int $product_category_id,
        ?bool $publish_unrecognized_offers = null,
        ?int $lead_content_id = null,
        ?int $lead_developer_id = null
    ): ProductTypeDto;

    /**
     * Отредактировать тип товара
     *
     * @param int $id
     * @param ?string $name
     * @param ?string $title
     * @param ?string $slug
     * @param ?int $product_category_id
     * @param ?array $synonyms
     * @param ?string $stage
     * @param ?bool $publish_unrecognized_offers
     * @param ?int $lead_content_id
     * @param ?int $lead_developer_id
     *
     * @return ProductTypeDto
     *
     * @throws \App\Exceptions\EntityNotFoundException
     * @throws \App\Exceptions\EntityValidationException
     * @throws \App\Exceptions\EntityNotUpdatedException
     */
    public function update(
        int $id,
        ?string $name = null,
        ?string $title = null,
        ?string $slug = null,
        ?int $product_category_id = null,
        ?array $synonyms = null,
        ?string $stage = null,
        ?bool $publish_unrecognized_offers = null,
        ?int $lead_content_id = null,
        ?int $lead_developer_id = null
    ): ProductTypeDto;

    /**
     * Удалить тип товара
     *
     * @param int $id
     *
     * @return void
     *
     * @throws \App\Exceptions\EntityNotFoundException
     * @throws \App\Exceptions\EntityNotDeletedException
     */
    public function delete(int $id): void;
}
