<?php

declare(strict_types=1);

namespace Products\Contracts;

use Products\Contracts\DataTransferObjects\ProductPropValueDto;

interface ProductPropValueServiceContract
{
    /**
     * Получить значение характеристики по ID
     *
     * @param int $id
     *
     * @return ProductPropValueDto
     *
     * @throws \App\Exceptions\EntityValidationException
     */
    public function getById(int $id): ProductPropValueDto;

    /**
     * Создать значение характеристики товара
     *
     * @param int $productPropId
     * @param ?string $value
     * @param ?string $title
     * @param ?array $synonyms
     *
     * @return ProductPropValueDto
     *
     * @throws \App\Exceptions\EntityValidationException
     * @throws \App\Exceptions\EntityNotCreatedException
     */
    public function create(
        int $productPropId,
        ?string $value,
        ?string $title = null,
        ?array $synonyms = null
    ): ProductPropValueDto;

    /**
     * Получить список значений одной характеристики товара
     *
     * @param int $productPropId
     *
     * @return ProductPropValueDto[]
     *
     * @throws \App\Exceptions\EntityNotFoundException;
     */
    public function listPropValues(
        int $productPropId
    ): array;

    /**
     * Получить список значений нескольких характеристик товара
     *
     * @param int[] $productPropIds
     *
     * @return array
     */
    public function listPropsValues(
        array $productPropIds
    ): array;

    /**
     * Отредактировать значение характеристики товара
     *
     * @param int $id
     * @param ?string $value
     * @param ?string $title
     * @param ?array $synonyms
     *
     * @return ProductPropValueDto
     *
     * @throws \App\Exceptions\EntityNotFoundException
     * @throws \App\Exceptions\EntityValidationException
     * @throws \App\Exceptions\EntityNotUpdatedException
     */
    public function update(
        int $id,
        ?string $value = null,
        ?string $title = null,
        ?array $synonyms = null
    ): ProductPropValueDto;

    /**
     * Удалить значение одной характеристики товара
     *
     * @param int $id
     *
     * @return void
     *
     * @throws \App\Exceptions\EntityNotFoundException
     * @throws \App\Exceptions\EntityNotDeletedException
     */
    public function delete(int $id): void;

    /**
     * Удалить несколько значений характеристик товара
     *
     * @param array $ids
     *
     * @return void
     *
     * @throws \App\Exceptions\EntityNotDeletedException
     */
    public function deleteMultiple(array $ids): void;
}
