<?php

declare(strict_types=1);

namespace Products\Contracts;

use App\Contracts\DataTransferObjects\PaginatedListDto;
use Products\Contracts\DataTransferObjects\ProductCategoryDto;
use Products\Contracts\DataTransferObjects\ProductPropDto;

interface ProductPropServiceContract
{
    /**
     * Получить характеристику по ID
     *
     * @param int $id
     *
     * @return ProductPropDto
     *
     * @throws \App\Exceptions\EntityNotFoundException
     */
    public function getById(int $id): ProductPropDto;

    /**
     * Получить список характеристик товара
     *
     * @param int|string $productTypeId ID или системное название типа товаров
     * @param ?string $searchQuery
     * @param ?bool $isPricelistProp
     * @param ?bool $isModelProp
     * @param ?bool $isFilterProp
     * @param string $orderBy
     *
     * @return ProductPropDto[]
     *
     * @throws \App\Exceptions\EntityNotFoundException
     */
    public function list(
        int|string $productTypeId,
        ?string $searchQuery = null,
        ?bool $isPricelistProp = null,
        ?bool $isModelProp = null,
        ?bool $isFilterProp = null,
        string $orderBy = 'order'
    ): array;

    /**
     * Создать характеристику товара
     *
     * @param int $product_type_id ID типа товара
     * @param ?string $title
     * @param ?string $slug Название шорткода для подстановки значений и GET-параметра в фильтрах
     * @param ?string $type Значения из config/models:product_props.types: string, enum, int, decimal, bool
     * @param ?bool $is_multiple Для enum-типа: может ли товар иметь несколько значений сразу
     * @param ?int $decimal_numbers Для decimal-типа: кол-во десятичных знаков
     * @param ?string $validation_regexp Для string-типа: регулярное выражение для валидации
     * @param ?float $min_value Для int/decimal-типов: минимальное допустимое значение
     * @param ?float $max_value Для int/decimal-типов: максимальное допустимое значение
     * @param ?bool $is_pricelist_prop Возможно ли получить значение из прайс-листа?
     * @param ?array $synonyms Синонимы обозначения характеристики в прайс-листе
     * @param ?bool $required_for_offer Должно ли значение быть распознано в прайс-листе и валидно, чтобы предложение
     *     считалось правильно распознанным?
     * @param ?bool $is_model_prop Является ли характеристикой модели? (Т.е. для всех предложений модели оно будет
     *     одинаковым.)
     * @param ?bool $required_for_model Должно ли значение быть заполнено и валидно, чтобы карточку товара можно было
     *     опубликовать?
     * @param ?bool $is_filter_prop Выводить ли фильтр по характеристике?
     * @param ?bool $is_parent_tag Может ли тег по характеристике содержать вложенные теги?
     * @param ?string $filter_title Название фильтра
     * @param ?string $filter_options_url Если значения не относятся ни к предложению, ни к товару, то с какого URL
     *     получать список значений?
     * @param ?string $filter_options_layout Способ отображения вариантов фильтра: checkboxes, dropdown (значения из
     *     config/models:product_props:filter_options_layouts)
     * @param ?bool $filter_has_search Выводить ли в фильтре строку поиска по вариантам?
     * @param ?string $filter_options_order Вид сортировки вариантов фильтра: alphabet, number (значение из
     *     config/models:filter_options_orders)
     * @param ?string $filter_hint Подсказка к фильтру
     *
     * @return ProductPropDto
     *
     * @throws \App\Exceptions\EntityValidationException
     * @throws \App\Exceptions\EntityNotCreatedException
     */
    public function create(
        int $product_type_id,
        ?string $title,
        ?string $slug,
        ?string $type,
        ?bool $is_multiple = null,
        ?int $decimal_numbers = null,
        ?string $validation_regexp = null,
        ?float $min_value = null,
        ?float $max_value = null,
        ?bool $is_pricelist_prop = null,
        ?array $synonyms = [],
        ?bool $required_for_offer = null,
        ?bool $is_model_prop = null,
        ?bool $required_for_model = null,
        ?bool $is_filter_prop = null,
        ?bool $is_parent_tag = null,
        ?string $filter_title = null,
        ?string $filter_options_url = null,
        ?string $filter_options_layout = null,
        ?bool $filter_has_search = null,
        ?string $filter_options_order = null,
        ?string $filter_hint = null
    ): ProductPropDto;

    /**
     * Отредактировать характеристику товара
     *
     * @param int $id
     * @param ?string $title
     * @param ?string $slug
     * @param ?string $type
     * @param ?bool $is_multiple
     * @param ?int $decimal_numbers
     * @param ?string $validation_regexp
     * @param ?int $min_value
     * @param ?int $max_value
     * @param ?bool $is_pricelist_prop
     * @param ?array $synonyms
     * @param ?bool $required_for_offer
     * @param ?bool $is_model_prop
     * @param ?bool $required_for_model
     * @param ?bool $is_filter_prop
     * @param ?bool $is_parent_tag
     * @param ?string $filter_title
     * @param ?string $filter_options_url
     * @param ?string $filter_options_layout
     * @param ?bool $filter_has_search
     * @param ?string $filter_options_order
     * @param ?string $filter_hint
     *
     * @return ProductPropDto
     *
     * @throws \App\Exceptions\EntityNotFoundException
     * @throws \App\Exceptions\EntityValidationException
     * @throws \App\Exceptions\EntityNotUpdatedException
     */
    public function update(
        int $id,
        ?string $title,
        ?string $slug,
        ?string $type,
        ?bool $is_multiple = null,
        ?int $decimal_numbers = null,
        ?string $validation_regexp = null,
        ?int $min_value = null,
        ?int $max_value = null,
        ?bool $is_pricelist_prop = null,
        ?array $synonyms = [],
        ?bool $required_for_offer = null,
        ?bool $is_model_prop = null,
        ?bool $required_for_model = null,
        ?bool $is_filter_prop = null,
        ?bool $is_parent_tag = null,
        ?string $filter_title = null,
        ?string $filter_options_url = null,
        ?string $filter_options_layout = null,
        ?bool $filter_has_search = null,
        ?string $filter_options_order = null,
        ?string $filter_hint = null
    ): ProductPropDto;

    /**
     * Удалить характеристику товаров
     *
     * @param int $id
     *
     * @return void
     *
     * @throws \App\Exceptions\EntityNotFoundException
     * @throws \App\Exceptions\EntityNotDeletedException
     */
    public function delete(int $id): void;
}
