<?php

declare(strict_types=1);

namespace Products\Contracts;

use App\Contracts\DataTransferObjects\PaginatedListDto;
use Products\Contracts\DataTransferObjects\ProductCategoryDto;

interface ProductCategoryServiceContract
{
    /**
     * Получить категорию товаров по ID
     *
     * @param int $id
     *
     * @return ProductCategoryDto
     *
     * @throws \App\Exceptions\EntityNotFoundException
     */
    public function getById(int $id): ProductCategoryDto;

    /**
     * Получить список категорий товаров
     *
     * @param int $perPage
     * @param array $linksQueryString Дополнительные GET-параметры, которые нужно добавить к ссылкам
     *
     * @return PaginatedListDto
     */
    public function list(
        int $perPage = 25,
        array $linksQueryString = []
    ): PaginatedListDto;

    /**
     * Получить дерево категорий товаров.
     */
    public function getTree(): array;

    /**
     * Создать категорию товара
     *
     * @param ?string $title
     * @param ?int $lead_sales_id
     *
     * @return ProductCategoryDto
     *
     * @throws \App\Exceptions\EntityValidationException
     * @throws \App\Exceptions\EntityNotCreatedException
     */
    public function create(
        ?string $title,
        ?int $lead_sales_id = null
    ): ProductCategoryDto;

    /**
     * Отредактировать категорию товара
     *
     * @param int $id
     * @param ?string $title
     * @param ?int $lead_sales_id
     *
     * @return ProductCategoryDto
     *
     * @throws \App\Exceptions\EntityNotFoundException
     * @throws \App\Exceptions\EntityValidationException
     * @throws \App\Exceptions\EntityNotUpdatedException
     */
    public function update(
        int $id,
        ?string $title,
        ?int $lead_sales_id = null
    ): ProductCategoryDto;

    /**
     * Переместить категорию товаров
     *
     * @param int $id Перемещаемся категория
     * @param int $target Категория, к которому перемещаем
     * @param string $relation Позиция относительно цели: 'before' / 'after' / 'inner'
     *
     * @return ProductCategoryDto
     *
     * @throws \App\Exceptions\EntityNotFoundException
     * @throws \App\Exceptions\EntityNotUpdatedException
     */
    public function move(
        int $id,
        int $target,
        string $relation
    ): ProductCategoryDto;

    /**
     * Удалить категорию товаров
     *
     * @param int $id
     *
     * @return void
     *
     * @throws \App\Exceptions\EntityNotFoundException
     * @throws \App\Exceptions\EntityNotDeletedException
     */
    public function delete(int $id): void;
}
