<?php

namespace Keywords\Domain\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;

class SearchKeyword extends Model
{
    public static function getWords(int $productTypeId, string $elmType, int $elmId)
    {
    }

    public static function setWords(int $productTypeId, string $elmType, int $elmId, array $words)
    {
        // Получаем список слов, чтобы посчитать разницу между нужным списком
        $availableElmTypes = config('models.search_keywords.elm_types', []);
        if (! array_key_exists($elmType, $availableElmTypes)) {
            throw new Exception('Неправильный тип элемента ключевика: ' . $elmType);
        }
        $oldSearchKeywords = SearchKeyword::query()
            ->where('product_type_id', $productTypeId)
            ->where('elm_type', $elmType)
            ->where('elm_id', $elmId)
            ->get();
        dd($oldSearchKeywords->toArray(), $words);
    }
}
