<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_keywords', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_type_id')->index();
            $types = config('models.search_keywords.elm_types', []);
            $table->enum('elm_type', array_keys($types))->default(array_key_first($types));
            // Связанный элемент: тип товара / намерение / характеристика / значение и тп
            $table->unsignedBigInteger('elm_id')->index();
            $table->string('word', 50);
            $table->string('norm_word', 50);
            $table->unique(['product_type_id', 'norm_word']);
            $table->unsignedSmallInteger('order')->default(1);
            $table->unsignedMediumInteger('pws')->nullable();
            $table->unsignedMediumInteger('ws')->nullable();
            $table->dateTime('ws_updated_at')->nullable();
            $table->unsignedBigInteger('author_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_keywords');
    }
};
