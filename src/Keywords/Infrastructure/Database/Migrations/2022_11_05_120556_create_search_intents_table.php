<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_intents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_type_id')->index();
            $table->string('title', 30);
            // Контексты использования и предлагаемые дополнения к URL (см config.models.search_intents.contexts)
            $table->json('contexts_slugs')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_intents');
    }
};
