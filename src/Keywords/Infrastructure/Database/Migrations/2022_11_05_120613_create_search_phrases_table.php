<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('search_phrases', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_type_id')->index();
            $table->string('phrase', 255);
            $table->string('norm_phrase', 255);
            $table->unsignedMediumInteger('pws')->nullable();
            $table->unsignedMediumInteger('ws')->nullable();
            $table->dateTime('ws_updated_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('search_phrases');
    }
};
